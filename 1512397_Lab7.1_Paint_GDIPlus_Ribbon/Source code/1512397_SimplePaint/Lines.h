#pragma once
#include "stdafx.h"
#include "Shapes.h"

class Lines : public Shapes
{
public:
	Lines();
	~Lines();

	void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
	void ReDraw(Graphics* graphics, Pen* pen);
	Shapes* CreateShape();
};

