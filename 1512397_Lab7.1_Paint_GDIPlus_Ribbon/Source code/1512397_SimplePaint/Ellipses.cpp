﻿#include "stdafx.h"
#include "Ellipses.h"

Ellipses::Ellipses()
{
}

Ellipses::~Ellipses()
{
}

void Ellipses::Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift)
{
	// Lưu thông số vẽ lại
	D_start = start;
	D_end = end;
	D_shift = shift;
	int width = abs(end.X - start.X);
	int height = abs(end.Y - start.Y);

	if (shift) {
		width = height = min(width, height);
		if (start.X > end.X)
			start.X = start.X - width;
		if (start.Y > end.Y)
			start.Y = start.Y - width;
	}
	else {
		if (start.X > end.X) start.X = end.X;
		if (start.Y > end.Y) start.Y = end.Y;
	}
	
	graphics->DrawEllipse(pen, start.X, start.Y, width, height);
}

void Ellipses::ReDraw(Graphics* graphics, Pen* pen)
{
	Draw(graphics, pen, D_start, D_end, D_shift);
}

Shapes* Ellipses::CreateShape()
{
	Shapes *shape = new Ellipses;
	return shape;
}
