﻿#include "stdafx.h"
#include "Lines.h"

Lines::Lines()
{
}

Lines::~Lines()
{
}

void Lines::Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift)
{
	// Lưu thông số vẽ lại
	D_start = start;
	D_end = end;
	D_shift = shift;

	graphics->DrawLine(pen, start, end);
}

void Lines::ReDraw(Graphics* graphics, Pen* pen)
{
	Draw(graphics, pen, D_start, D_end, D_shift);
}

Shapes* Lines::CreateShape()
{
	Shapes* shape = new Lines;
	return shape;
}
