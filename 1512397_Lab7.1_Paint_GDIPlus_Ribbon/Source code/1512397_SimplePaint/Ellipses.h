#pragma once
#include "Shapes.h"

class Ellipses: public Shapes
{
public:
	Ellipses();
	~Ellipses();
	void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
	void ReDraw(Graphics* graphics, Pen* pen);
	Shapes* CreateShape();
};

