//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512397SimplePaint.rc
//
#define IDC_MYICON                      2
#define IDD_1512397SIMPLEPAINT_DIALOG   102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512397SIMPLEPAINT          107
#define IDI_SMALL                       108
#define IDC_1512397SIMPLEPAINT          109
#define IDC_STATUSBAR                   110
#define IDR_MAINFRAME                   128
#define IDI_PAINT                       129
#define ID_SHAPES_LINE                  32771
#define ID_SHAPES_RECTANGLE             32772
#define ID_SHAPES_ELLIPSE               32773
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
