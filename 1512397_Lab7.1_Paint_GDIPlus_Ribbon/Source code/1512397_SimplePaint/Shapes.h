#pragma once
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

class Shapes
{
protected:
	Point D_start;
	Point D_end;
	bool D_shift;
public:
	Shapes();
	~Shapes();

	virtual void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift) = 0;
	virtual void ReDraw(Graphics* graphics, Pen* pen) = 0;
	virtual Shapes* CreateShape() = 0;
};