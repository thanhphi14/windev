#pragma once
#include "Shapes.h"

class Rectangles: public Shapes
{
public:
	Rectangles();
	~Rectangles();

	void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
	void ReDraw(Graphics* graphics, Pen* pen);
	Shapes* CreateShape();
};

