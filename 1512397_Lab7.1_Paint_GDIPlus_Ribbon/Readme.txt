** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Sử dụng Ribbon để tạo giao diện
	- Sử dụng bitblt để cho ứng dụng không dựt
	- Vẽ được các hình vẽ cơ bản:
		+ Đường thẳng
		+ Hình chữ nhật
		+ Hình vuông (Khi nhấn đồng thời phím Shift)
		+ Hình ellipse
		+ Hình tròn (Khi nhấn đồng thời phím Shift)
	- Sử dụng được kỹ thuật Hàm mẫu (Prototype)
	- Sử dụng được kỹ thuật Đa xạ (Tạo lớp Shape bao bọc các Hình vẽ, Lưu các hình đã được vẽ lại - Factory)
	- Tạo menu Shape chọn loại hình muốn vẽ (Line, Rectangle, Ellipse)
	- Khi chọn 1 loại hình vẽ trong menu thì sẽ Check vào loại hình được chọn 
	
** Mainflow:
	- Khi mở chương trình, mặc định sẽ vẽ đường thẳng
	- Nếu muốn vẽ hình khác thì chọn vào các button tương ứng trên Ribbon
		+ Rectangle (Hình chữ nhật)
		+ Ellipse (hình ellipse)
		+ Line (Đường thẳng)
	- Nếu muốn vẽ hình vuông thì chọn Rectangle sau đó vừa vẽ vừa nhấn phím Shift
	- Tương tự hình tròn thì chọn Ellipse
		
** Additional flow:	
	- Khi vẽ 1 loại hình bất kì nào đó thì thanh Status Bar sẽ hiển thị loại hình được chọn để vẽ
	- Vẽ được bao nhiêu hình thì Status Bar sẽ hiển thị số lượng hình đã được vẽ
	- Thanh Status Bar hiển thị vị trí chuột hiện tại ở cửa sổ Client
	
** Link Youtube:
	https://youtu.be/RPByrsb1GXY
	
** Link Reponsitory:
	https://thanhphi14@bitbucket.org/thanhphi14/windev.git