
** My info:
	+ Student ID: 1512397
	+ Name: Ngô Thanh Phi
	+ Email: thanhphi.dd@gmail.com
** I have done:
	+ Đã hoàn thành yêu cầu bài tập: Dựa vào số liệu (giá cả) mà người dùng nhập vào 2 ô Nhật và Singapore thì 
	  sẽ tự động cho ra kết quả nên mua ở đâu sẽ rẻ hơn hoặc cả 2 đều bằng giá nhau.
	+ Lưu ý: Cả 2 loại tiền tệ trên đều được đổi sang tiền Việt Nam để so sánh. Giá mới cập nhật ngày 20/9/2017.
** Mainflow:
	Khi điền giá (kiểu sô nguyên hoặc kiểu số thực) vào 2 textbox tương ứng (Giá Nhật, Giá Singapore) và nhấp
	vào nút "Kết quả" thì sẽ thấy xuất hiện dòng chữ màu ĐỎ "Mua ở Nhật (Singapore) đi bạn vì Nhật (Singapore)
	rẻ hơn!"  hay "Ở đâu cũng rẻ hơn Việt Nâm nha ^^".
** Additional flow:
	+ Nếu người dùng bỏ trống (không điền giá vào hộp thoại) thì sẽ xuất hiện hộp thoại báo lỗi "Bạn phải nhập 
	  giá tiền ở Nhật (Singapore) vào", người dùng phải bấm OK để nhập lại.
	+ Nếu người dùng điền giá trị âm (-) thì sẽ xuất hiện hộp thoại báo lỗi "Giá tiền ở Nhật (Singapore) phải 
	  là số DƯƠNG", người dùng phải bấm OK để nhập lại.
	+ Nếu người dùng điền giá tiền không chính xác (chữ, ký tự đặc biệt,...) thì sẽ xuất hiện hộp thoại báo lỗi 
	  "Giá tiền ở Nhật (Singapore) phải là một số cụ thể", người dùng phải bấm OK để nhập lại.
		