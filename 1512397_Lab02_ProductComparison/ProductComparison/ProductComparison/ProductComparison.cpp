﻿// ProductComparison.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "ProductComparison.h"
#include <windowsX.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100
HFONT deFont(LOGFONT lf, int size);
bool isNumber(WCHAR*, int);
bool isEmpty(int);
bool isNegative(WCHAR*);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PRODUCTCOMPARISON, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PRODUCTCOMPARISON));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MICON));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PRODUCTCOMPARISON);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MICON));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowEx(0, szWindowClass, L"Săn Ipad",
	   WS_OVERLAPPEDWINDOW, 400, 180, 600, 400, NULL, NULL, hInstance, NULL);
  
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);

	case WM_CTLCOLORSTATIC:
		// Set the colour of the text for our URL
		if ((HWND)lParam == GetDlgItem(hWnd, IDC_RESULT))
			SetTextColor((HDC)wParam, RGB(205, 55, 0));
		SetBkMode((HDC)wParam, TRANSPARENT);
		return (BOOL)CreateSolidBrush(GetSysColor(COLOR_BTNFACE));

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

HWND txtJPY;
HWND txtSGD;
HWND txtResult;

HFONT deFont(LOGFONT lf, int size)
{
	HFONT hFont = CreateFont(size, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	return hFont;
}

bool isNumber(WCHAR* text, int size)
{
	int flag = 0;
	if (text[0] == '.')
		return false;
	for (int i = 0; i < size; i++) {
		if (text[i] == '.')
			flag++;
		if (flag > 1)
			return false;
		if (!(('0' <= text[i] && text[i] <= '9') || text[i] == '.'))
			return false;
	}
	return true;
}

bool isEmpty(int size)
{
	if (size == 0)
		return true;
	return false;
}

bool isNegative(WCHAR* text)
{
	if (text[0] == '-')
		return true;
	return false;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = deFont(lf, 18);
	HFONT hFont1 = deFont(lf, 25);
	HFONT hFont2 = deFont(lf, 20);

	HWND hwnd = CreateWindowEx(0, L"STATIC", L"SĂN IPAD GIÁ RẺ", WS_CHILD | WS_VISIBLE | SS_LEFT, 220, 10, 300, 30, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont1), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Giá Nhật (JPY):", WS_CHILD | WS_VISIBLE | SS_LEFT, 10, 50, 100, 18, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hwnd = CreateWindowEx(0, L"STATIC", L"Giá Singapore (SGD):", WS_CHILD | WS_VISIBLE | SS_LEFT, 10, 95, 135, 18, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hwnd = CreateWindowEx(0, L"BUTTON", L"Kết quả:", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 140, 120, 30, hWnd, (HMENU)IDC_MYBUTTON, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	txtJPY = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 160, 50, 180, 22, hWnd, NULL, hInst, NULL);
	SendMessage(txtJPY, WM_SETFONT, WPARAM(hFont), TRUE);

	txtSGD = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 160, 95, 180, 22, hWnd, NULL, hInst, NULL);
	SendMessage(txtSGD, WM_SETFONT, WPARAM(hFont), TRUE);

	txtResult = CreateWindowEx(0, L"STATIC", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER, 10, 180, 330, 25, hWnd, (HMENU)IDC_RESULT, hInst, NULL);
	SendMessage(txtResult, WM_SETFONT, WPARAM(hFont2), TRUE);

	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	WCHAR* bufferJPY = NULL;
	WCHAR* bufferSGD = NULL;
	WCHAR* bufferRS = NULL;
	WCHAR* bufferERROR = NULL;

	int sizeJPY;
	int sizeSGD;

	double JPY;
	double SGD;
	double mediate;

	bool errorSGD = false;
	bool errorJPY = false;

	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	case IDC_MYBUTTON:
		sizeJPY = GetWindowTextLength(txtJPY);
		sizeSGD = GetWindowTextLength(txtSGD);

		bufferJPY = new WCHAR[sizeJPY + 1];
		bufferSGD = new WCHAR[sizeSGD + 1];

		GetWindowText(txtJPY, bufferJPY, sizeJPY + 1);
		GetWindowText(txtSGD, bufferSGD, sizeSGD + 1);

		bufferERROR = new WCHAR[255];

		if (isEmpty(sizeJPY)) {
			wsprintf(bufferERROR, L"Bạn phải nhập giá tiền ở Nhật vào!\n");
			errorJPY = true;
		}
		else if (isNegative(bufferJPY)) {
			wsprintf(bufferERROR, L"Giá tiền ở Nhật phải là số DƯƠNG!\n");
			errorJPY = true;
		}
		else if(!isNumber(bufferJPY, sizeJPY)) {
			wsprintf(bufferERROR, L"Giá tiền ở Nhật phải là một số cụ thể!\n");
			errorJPY = true;
		}

		if (isEmpty(sizeSGD)) {
			if(errorJPY) wsprintf(bufferERROR +lstrlen(bufferERROR), L"Bạn phải nhập giá tiền ở Singapore vào!\n");
			else wsprintf(bufferERROR, L"Bạn phải nhập giá tiền ở Singapore vào!\n");
			errorSGD = true;
		}
		else if (isNegative(bufferSGD)) {
			if (errorJPY) wsprintf(bufferERROR + lstrlen(bufferERROR), L"Giá tiền ở Singapore phải là số DƯƠNG!\n");
			else wsprintf(bufferERROR, L"Giá tiền ở Singapore phải là số DƯƠNG!\n");
			errorSGD = true;
		}
		else if (!isNumber(bufferSGD, sizeSGD)) {
			if (errorJPY) wsprintf(bufferERROR + lstrlen(bufferERROR), L"Giá tiền ở Singapore phải là một số cụ thể!\n");
			else wsprintf(bufferERROR, L"Giá tiền ở Singapore phải là một số cụ thể!\n");
			errorSGD = true;
		}

		if (errorSGD || errorJPY) {
			SetWindowText(txtResult, L"");
			MessageBox(hWnd, bufferERROR, L"Lỗi:", MB_OK);
			break;
		}

		JPY = _wtof(bufferJPY);
		SGD = _wtof(bufferSGD);

		mediate = SGD * 15890 - JPY * 204;

		bufferRS = new WCHAR[255];
		if (mediate > 0)
			wsprintf(bufferRS, L"Mua ở Nhật đi bạn vì Nhật rẻ hơn!");
		else if (mediate < 0)
			wsprintf(bufferRS, L"Mua ở Singapore đi bạn vì Singapore rẻ hơn!");
		else
			wsprintf(bufferRS, L"Ở đâu cũng rẻ hơn Việt Nam nha ^^");

		SetWindowText(txtResult, bufferRS);

		break;
	}

	if (!bufferJPY)
		delete[] bufferJPY;
	if (!bufferSGD)
		delete[] bufferSGD;
	if (!bufferRS)
		delete[] bufferRS;
	if (!bufferERROR)
		delete[] bufferERROR;
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}