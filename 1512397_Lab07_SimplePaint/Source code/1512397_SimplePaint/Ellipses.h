#pragma once
#include "Shapes.h"

class Ellipses: public Shapes
{
public:
	Ellipses();
	~Ellipses();
	void Draw(HWND hWnd, POINT start, POINT end);
	void ReDraw(HWND hWnd);
	Shapes* CreateShape();
};

