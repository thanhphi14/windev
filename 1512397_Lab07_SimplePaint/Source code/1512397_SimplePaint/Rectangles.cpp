﻿#include "stdafx.h"
#include "Rectangles.h"

Rectangles::Rectangles()
{
}

Rectangles::~Rectangles()
{
}

void Rectangles::Draw(HWND hWnd, POINT start, POINT end)
{
	/*POINT square;
	if (start.x < end.x)
		square.x = start.x + abs(start.y - end.y);
	else
		square.x = start.x - abs(start.y - end.y);
	square.y = end.y;*/

	// Lưu thông số vẽ lại
	D_start = start;
	D_end = end;

	HDC hdc = GetDC(hWnd);
	SelectObject(hdc, GetStockObject(NULL_BRUSH)); // Trong suốt
	Rectangle(hdc, start.x, start.y, end.x, end.y);

	ReleaseDC(hWnd, hdc);
}

void Rectangles::ReDraw(HWND hWnd)
{
	Draw(hWnd, D_start, D_end);
}

Shapes* Rectangles::CreateShape()
{
	Shapes *shape = new Rectangles;
	return shape;
}
