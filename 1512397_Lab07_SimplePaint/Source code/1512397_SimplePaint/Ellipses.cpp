﻿#include "stdafx.h"
#include "Ellipses.h"

Ellipses::Ellipses()
{
}

Ellipses::~Ellipses()
{
}

void Ellipses::Draw(HWND hWnd, POINT start, POINT end)
{
	// Lưu thông số vẽ lại
	D_start = start;
	D_end = end;

	HDC hdc = GetDC(hWnd);

	SelectObject(hdc, GetStockObject(NULL_BRUSH)); // Trong suốt
	Ellipse(hdc, start.x, start.y, end.x, end.y);

	ReleaseDC(hWnd, hdc);
}

void Ellipses::ReDraw(HWND hWnd)
{
	Draw(hWnd, D_start, D_end);
}


Shapes* Ellipses::CreateShape()
{
	Shapes *shape = new Ellipses;
	return shape;
}
