#pragma once
#include "stdafx.h"
#include "Shapes.h"

class Lines : public Shapes
{
public:
	Lines();
	~Lines();

	void Draw(HWND hWnd, POINT start, POINT end);
	void ReDraw(HWND hWnd);
	Shapes* CreateShape();
};

