﻿#include "stdafx.h"
#include "Lines.h"

Lines::Lines()
{
}

Lines::~Lines()
{
}

void Lines::Draw(HWND hWnd, POINT start, POINT end)
{
	// Lưu thông số vẽ lại
	D_start = start;
	D_end = end;

	HDC hdc = GetDC(hWnd);
	MoveToEx(hdc, start.x, start.y, NULL);
	LineTo(hdc, end.x, end.y);

	ReleaseDC(hWnd, hdc);
}

void Lines::ReDraw(HWND hWnd)
{
	Draw(hWnd, D_start, D_end);
}

Shapes* Lines::CreateShape()
{
	Shapes* shape = new Lines;
	return shape;
}
