#pragma once

class Shapes
{
protected:
	POINT D_start;
	POINT D_end;
public:
	Shapes();
	~Shapes();

	virtual void Draw(HWND hWnd, POINT lefttop, POINT rightbottom) = 0;
	virtual void ReDraw(HWND hWnd) = 0;
	virtual Shapes* CreateShape() = 0;
};