#pragma once
#include "Shapes.h"

class Rectangles: public Shapes
{
public:
	Rectangles();
	~Rectangles();

	void Draw(HWND hWnd, POINT start, POINT end);
	void ReDraw(HWND hWnd);
	Shapes* CreateShape();
};

