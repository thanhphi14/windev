﻿// 1512397SimplePaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512397SimplePaint.h"
#include <windowsx.h>
#include "Lines.h"
#include "Ellipses.h"
#include "Rectangles.h"
#include <commctrl.h>
#include <vector>
#pragma comment(lib, "ComCtl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
using namespace std;

#define MAX_LOADSTRING 100
#define LINE 0
#define RECTANGLE 1
#define ELLIPSE 2
#define HEIGHTSTATUSBAR 23

// Lưu nhũng hình đã vẽ
vector <Shapes*> FactoryShape;
// Kỹ thuật vẽ hàng mẫu
vector <Shapes*> PrototypeShape;

// Điểm bắt đầu, kết thúc của mỗi hình vẽ
POINT g_start;
POINT g_current;
POINT g_end;

// Các thông số kiểm tra khác
BOOL g_IsDrawingPreview = FALSE;
int IndexShapeType;
int LastShape;
BOOL flagShift = FALSE;

HWND statusBar;

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512397SIMPLEPAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512397SIMPLEPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512397SIMPLEPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_PAINT));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_KEYDOWN:
		if (wParam == VK_SHIFT)
			flagShift = TRUE;
		break;
	case WM_KEYUP:
		if (wParam == VK_SHIFT)
			flagShift = FALSE;
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Khởi tạo Protopyte
	PrototypeShape.push_back(new Lines);
	PrototypeShape.push_back(new Rectangles);
	PrototypeShape.push_back(new Ellipses);

	// Tạo statusbar
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);
	int nStatusSize[3] = { 100, 220, -1 };
	statusBar = CreateWindowEx(0, STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, (HMENU)IDC_STATUSBAR, hInst, NULL);
	SendMessage(statusBar, SB_SETPARTS, 3, (LPARAM)&nStatusSize);

	// Check Menu được chọn
	HMENU hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_CHECKED);

	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)L"Tổng hình: 0");
	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	// Khởi tạo các thông số cơ bản
	HMENU hMenu = GetMenu(hWnd);
	switch (id)
	{
	case ID_SHAPES_LINE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		g_IsDrawingPreview = FALSE;
		IndexShapeType = LINE;
		break;
	case ID_SHAPES_RECTANGLE:
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		g_IsDrawingPreview = FALSE;
		IndexShapeType = RECTANGLE;
		break;
	case ID_SHAPES_ELLIPSE:
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		g_IsDrawingPreview = FALSE;
		IndexShapeType = ELLIPSE;
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	// Thay đổi kích thước Status Bar
	MoveWindow(statusBar, 0, 0, 0, 0, TRUE);
}

void OnLButtonDown(HWND hWnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	//Nếu chưa bắt đầu vẽ thì tạo ra một hình mới
	if (!g_IsDrawingPreview)
		FactoryShape.push_back(PrototypeShape[IndexShapeType]->CreateShape());

	// Bắt đầu vẽ
	g_IsDrawingPreview = TRUE;
	g_start.x = x;
	g_start.y = y;

	// Set đường biên
	SetCapture(hWnd);

	if (IndexShapeType ==0)
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Đường thẳng");
}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags) // x, y: Tọa độ chuột
{ 
	// Lấy thông số cửa sổ chính
	RECT rectWin;
	GetClientRect(hWnd, &rectWin);

	// Thông số hình vuông
	if (flagShift && IndexShapeType != 0) {
		if (g_start.x < x)
			g_current.x = g_start.x + abs(g_start.y - y);
		else
			g_current.x = g_start.x - abs(g_start.y - y);
	}
	else g_current.x = x; 

	// Quá Status Bar
	g_current.y = y;
	if (g_current.y > rectWin.bottom - HEIGHTSTATUSBAR)
		g_current.y = rectWin.bottom - HEIGHTSTATUSBAR;

	// Vẽ hình xem trước
	if (g_IsDrawingPreview) {
		LastShape = FactoryShape.size() - 1;
		FactoryShape[LastShape]->Draw(hWnd, g_start, g_current);

		InvalidateRect(hWnd, NULL, TRUE); 

		// Status Bar
		if (IndexShapeType == 1 && flagShift)
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình vuông");
		if (IndexShapeType == 2 && flagShift)
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình tròn");
		if (IndexShapeType == 1 && !flagShift)
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình chữ nhật");
		if (IndexShapeType == 2 && !flagShift)
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình ellipse");
	}

	WCHAR buffer[20];
	wsprintf(buffer, L" %d, %dpx", x, y);
	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer);
}

void OnLButtonUp(HWND hWnd, int x, int y, UINT keyFlags)
{
	// Lấy thông sô cửa sổ chính, quá Status Bar
	RECT rectWin;
	GetClientRect(hWnd, &rectWin);
	if (flagShift && IndexShapeType != 0) {
		if (g_start.x < x)
			g_end.x = g_start.x + abs(g_start.y - y);
		else
			g_end.x = g_start.x - abs(g_start.y - y);
	}
	else g_end.x = x;
	g_end.y = y;
	if (g_end.y > rectWin.bottom - HEIGHTSTATUSBAR)
		g_end.y = rectWin.bottom - HEIGHTSTATUSBAR;

	// Vẽ hình hiện tại
	LastShape = FactoryShape.size() - 1;
	FactoryShape[LastShape]->Draw(hWnd, g_start, g_end);

	// Hủy đang vẽ
	g_IsDrawingPreview = FALSE;
	InvalidateRect(hWnd, NULL, TRUE);
	ReleaseCapture();

	WCHAR buffer[20];
	wsprintf(buffer, L"Tổng hình: %d", LastShape + 1);
	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)buffer);
	if (IndexShapeType == 1)
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình chữ nhật");
	if (IndexShapeType == 2)
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình ellipse");
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	// Vẽ lại toàn bộ hình đã có
	for (int i = 0; i < FactoryShape.size(); ++i)
		FactoryShape[i]->ReDraw(hWnd);

	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}