** My info:
	+ Student ID: 1512397
	+ Name: Ngô Thanh Phi
	+ Email: thanhphi.dd@gmail.com
** I have done:
	+ Tạo được giao diện có một edit control để nhập liệu.
	+ Có sử dụng SaveFileDialog. Lưu dưới dạng file text. 
	+ Có sử dụng OpenFileDialog. Mở một tập tin, đọc và hiển thị nội dung để chỉnh sửa tiếp.
	+ Hỗ trợ 3 thao tác Cut - Copy - Paste.
** Mainflow:
	Thực hiện mở file (File -> Open) hoặc ghi trực tiếp vào giao diện mới mở. Sau khi hoàn tất quá trình ghi 
	(chỉnh sửa) thì lưu file tại Save (File -> Save) hoặc Save as... (File -> Save As...) vậy là hoàn tất quá 
	trình đọc và ghi file.
** Additional flow:
	+ Nếu người dùng lỡ tay làm sai 1 thao tác thì có thể chọn Undo (Edit -> Undo) để hoàn lại thao tác vừa rồi.
	+ Nếu người dùng muốn chọn tất cả thì có thao tác Select All (Edit -> Select All).
	+ Nếu người dùng mở 1 giao diện mới vào ghi dữ liệu vào sau đó bấm thoát (File -> Exit) mà chưa lưu thì 
	  sẽ xuất hiện hộp thoại lưu file. 
	+ Nếu người dùng muốn xóa dữ liệu thì chọn vùng cần xóa sau đó vào Edit -> Delete.
	+ Nếu người dùng muốn tạo 1 giao diện text mới thì vào File -> New.
	+ Chỉ hỗ trợ ANSI.
		