﻿// 1512397Lab03.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512397Lab03.h"
#include <windowsx.h>
#include <fstream>
#include <string>
#include <commdlg.h>
#include <codecvt>
#include <locale>
using namespace std;
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100
#define DEFAULTWIDTH 685
#define DEFAULTHEIGHT 590

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
void OnSize(HWND hwnd, UINT state, int cx, int cy);

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

												// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_1512397LAB03, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512397LAB03));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MICON));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_1512397LAB03);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MICON));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		350, 80, DEFAULTWIDTH, DEFAULTHEIGHT, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

HWND txtEdit;

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(19, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	txtEdit = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | ES_MULTILINE | WS_HSCROLL | WS_VSCROLL,
		0, 0, 0, 0, hWnd, NULL, hInst, NULL);
	SendMessage(txtEdit, WM_SETFONT, WPARAM(hFont), TRUE);
	SetFocus(txtEdit);
	return true;
}

wstring OpenFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"Text Files (*.txt)\0*.txt\0All Files (.)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"txt";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

	if (GetOpenFileName(&ofn) == TRUE)
	{
		wstring wstr(szFileName);
		return wstr;
	}
	return L"";
}

wstring SaveFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"Text Files (*.txt)\0*.txt\0All Files (.)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"txt";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_NOTESTFILECREATE | OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&ofn) == TRUE)
	{
		wstring wstr(szFileName);
		return wstr;
	}

	return L"";
}

wstring stringToWstring(const string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	wstring r(buf);
	delete[] buf;
	return r;
}

int check_open = 0;
int check_save = 0;
wstring file_open, file_save;
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	fstream fo, fs;
	string buffer;
	string tembuf = "";
	wstring temp;
	LPCWSTR result;
	CHAR* buffer2;
	int length;

	switch (id)
	{
	case ID_FILE_NEW:
		SetWindowText(txtEdit, L"");
		break;
	case ID_FILE_OPEN:
		file_open = OpenFileDialog(hWnd);
		if (file_open != L"") {
			fo.open(file_open, ios::in | ios::out);
			while (!fo.eof()) {
				getline(fo, buffer);
				tembuf += buffer;
				if (!fo.eof())
					tembuf += "\r\n";
			}
			temp = stringToWstring(tembuf);
			result = temp.c_str();
			SetWindowText(txtEdit, result);
			check_open = 1;
		}
		break;
	case ID_FILE_SAVE:
		length = GetWindowTextLength(txtEdit);
		buffer2 = new CHAR[length];
		GetWindowTextA(txtEdit, buffer2, length + 1);
		if (check_open == 0 && check_save == 0)
			file_save = SaveFileDialog(hWnd);
		else if (check_open == 1)
			file_save = file_open;
		fs.open(file_save, ios::out);
		fs << buffer2;
		check_save = 1;
		break;
	case ID_FILE_SAVEAS:
		length = GetWindowTextLength(txtEdit);
		buffer2 = new CHAR[length];
		GetWindowTextA(txtEdit, buffer2, length + 1);
		file_save = SaveFileDialog(hWnd);
		fs.open(file_save, ios::out);
		fs << buffer2;
		break;
	case ID_EDIT_UNDO:
		SendMessage(txtEdit, WM_UNDO, 0, 0);
		break;
	case ID_EDIT_COPY:
		SendMessage(txtEdit, WM_COPY, 0, 0);
		break;
	case ID_EDIT_CUT:
		SendMessage(txtEdit, WM_CUT, 0, 0);
		break;
	case ID_EDIT_PASTE:
		SendMessage(txtEdit, WM_PASTE, 0, 0);
		break;
	case ID_EDIT_DELETE:
		SendMessage(txtEdit, WM_CLEAR, 0, 0);
		break;
	case ID_EDIT_SELECTALL:
		SendMessage(txtEdit, EM_SETSEL, 0, -1);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:		
		length = GetWindowTextLength(txtEdit);
		buffer2 = new CHAR[length];
		GetWindowTextA(txtEdit, buffer2, length + 1);
		if (check_open == 0 && check_save == 0)
			file_save = SaveFileDialog(hWnd);
		else if (check_open == 1)
			file_save = file_open;
		fs.open(file_save, ios::out);
		fs << buffer2;
		DestroyWindow(hWnd);
		break;
	}
	fo.close();
	fs.close();
}

void OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	SetWindowPos(txtEdit, NULL, 3, 0, cx - 3, cy, state);
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}