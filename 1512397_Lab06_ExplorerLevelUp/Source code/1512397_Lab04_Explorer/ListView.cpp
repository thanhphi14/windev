﻿#include "stdafx.h"
#include "ListView.h"

#define IDI_DRIVE 0

ListView::ListView()
{
	_hListView = NULL;
}

ListView::~ListView()
{
	DestroyWindow(_hListView);
}

void ListView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	INITCOMMONCONTROLSEX icex;

	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	_hParent = parentWnd;
	_hListView = CreateWindowEx(0, WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER |LVS_REPORT | LVS_EX_GRIDLINES, x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);
	InitTempCols();


	HIMAGELIST smallIcon;
	HIMAGELIST largeIcon;
	Shell_GetImageLists(&largeIcon, &smallIcon);
	
	ListView_SetImageList(_hListView, smallIcon, LVSIL_SMALL);
}

HWND ListView::_getHandle()
{
	return _hListView;
}

LPCWSTR ListView::_getPath(int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(_hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

void ListView::List_DeleteAll()
{
	ListView_DeleteAllItems(_hListView);
}

void ListView::InitTempCols()
{
	LVCOLUMN LvCol0;
	LvCol0.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol0.fmt = LVCFMT_LEFT;
	LvCol0.cx = 230;
	LvCol0.pszText = L"Tên";
	ListView_InsertColumn(_hListView, 0, &LvCol0);

	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_InsertColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_InsertColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_InsertColumn(_hListView, 3, &LvCol3);
}

void ListView::InitDriveCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_RIGHT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Tổng dung lượng";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 150;
	LvCol3.pszText = L"Dung lượng trống";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::InitFolderCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::List_LoadThisPc(Drive * _drive)
{
	InitDriveCols();
	LV_ITEM ListV;

	for (int i = 0; i < _drive->_getCount(); i++)
	{
		//Cột tên
		ListV.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		ListV.iItem = i;
		ListV.iSubItem = 0;
		ListV.pszText = _drive->_getDisplayName(i);
		ListV.iImage = IDI_DRIVE;
		ListV.lParam = (LPARAM)_drive->_getDriveName(i);
		ListView_InsertItem(_hListView, &ListV);

		//Các cột còn lại
		ListV.mask = LVIF_TEXT;

		//Cột Loại
		ListV.iSubItem = 1;
		ListV.pszText = _drive->_getTypeDisk(i);
		ListView_SetItem(_hListView, &ListV);

		//Cột Tổng Dung Lượng
		ListV.iSubItem = 2;
		if (_drive->_getDiskIndex(i) == DRIVE_FIXED)
			ListV.pszText = _drive->_getSize(i);
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);

		//Cột Dung Lượng Trống
		ListV.iSubItem = 3;
		if (_drive->_getDiskIndex(i) == DRIVE_FIXED)
			ListV.pszText = _drive->_getFreeSize(i);
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);
	}

	TCHAR *buffer2 = new TCHAR[34];
	wsprintf(buffer2, L"%d items", _drive->_getCount());
	SendMessage(GetDlgItem(_hParent, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer2);
}

void ListView::List_LoadFileandFolder(LPCWSTR path)
{
	if (path == NULL)
		return;

	InitFolderCols();
	List_DeleteAll();
	LV_ITEM lv;
	int nItemCount = 0;

	TCHAR *folderPath;
	int fileCount = 0;

	HRESULT hr;
	IShellFolder* pFolder = NULL;
	IEnumIDList* pEnumIds = NULL;
	LPITEMIDLIST pidl = NULL;
	LPITEMIDLIST pidlItem = NULL;
	WIN32_FIND_DATA fd;
	SHFILEINFO sfi;

	pidl = ILCreateFromPath(path);

	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)& pFolder);

	if (SUCCEEDED(hr))
	{
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds);
		if (SUCCEEDED(hr))
		{
			STRRET strDisplayName;
			while ((hr = pEnumIds->Next(1, &pidlItem, NULL)) == S_OK)
			{
				hr = pFolder->GetDisplayNameOf(pidlItem, SHGDN_NORMAL, &strDisplayName);
				SHGetDataFromIDList(pFolder, pidlItem, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
				SHGetFileInfo((LPCTSTR)pidlItem, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);
				if (SUCCEEDED(hr))
				{
					folderPath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
					StrCpy(folderPath, path);

					if (wcslen(path) != 3)
						StrCat(folderPath, L"\\");

					StrCat(folderPath, fd.cFileName);

					lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
					lv.iItem = nItemCount;
					lv.iSubItem = 0;
					lv.pszText = sfi.szDisplayName;
					lv.iImage = sfi.iIcon;
					lv.lParam = (LPARAM)folderPath;
					ListView_InsertItem(_hListView, &lv);

					//Cột Loại
					ListView_SetItemText(_hListView, nItemCount, 1, sfi.szTypeName);

					//Cột Ngày chỉnh sửa
					ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));
					++nItemCount;
					fileCount++;
				}
			}
		}


		hr = pFolder->EnumObjects(NULL, SHCONTF_NONFOLDERS, &pEnumIds);
		if (SUCCEEDED(hr))
		{
			STRRET strDisplayName;
			while ((hr = pEnumIds->Next(1, &pidlItem, NULL)) == S_OK)
			{
				hr = pFolder->GetDisplayNameOf(pidlItem, SHGDN_NORMAL, &strDisplayName);
				SHGetDataFromIDList(pFolder, pidlItem, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
				SHGetFileInfo((LPCTSTR)pidlItem, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);
				if (SUCCEEDED(hr))
				{
					folderPath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
					StrCpy(folderPath, path);

					if (wcslen(path) != 3)
						StrCat(folderPath, L"\\");

					StrCat(folderPath, fd.cFileName);

					lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
					lv.iItem = nItemCount;
					lv.iSubItem = 0;
					lv.pszText = sfi.szDisplayName;
					lv.iImage = sfi.iIcon;
					lv.lParam = (LPARAM)folderPath;
					ListView_InsertItem(_hListView, &lv);

					//Cột Loại
					ListView_SetItemText(_hListView, nItemCount, 1, sfi.szTypeName);

					//Cột Kích thước
					ListView_SetItemText(_hListView, nItemCount, 3, _getSize(fd));

					//Cột Ngày chỉnh sửa
					ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));
					++nItemCount;
					fileCount++;
				}
			}
		}
	}

	TCHAR *buffer2 = new TCHAR[34];
		wsprintf(buffer2, L"%d items", fileCount);
	SendMessage(GetDlgItem(_hParent, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer2);
}

void ListView::List_LoadChild(LPCWSTR path, Drive *drive)
{
	if (path == NULL)
		return;

	if (!StrCmp(path, L"This PC"))
		List_LoadThisPc(drive);
	else
		List_LoadFileandFolder(path);
}

void ListView::List_LoadFolderOrOpenFile(HWND parentWnd)
{
	LPCWSTR path = _getPath(ListView_GetSelectionMark(_hListView));
	WIN32_FIND_DATA fd;
	
	if (GetFileAttributesEx(path, GetFileExInfoStandard, &fd) != 0) {
		//Thư mục thì mở
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			ListView_DeleteAllItems(_hListView);
			List_LoadFileandFolder(path);
			SetDlgItemText(parentWnd, ID_ADDR, path);
		}
		else { //Tập tin thì chạy
			ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
		}
	}
}

LPWSTR ListView::_getDateModified(const FILETIME &ftLastWrite)
{
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	wchar_t *buffer = new wchar_t[20];
	wsprintf(buffer, L"%02d/%02d/%04d   %02d:%02d", stLocal.wDay, stLocal.wMonth, stLocal.wYear, stLocal.wHour, stLocal.wMinute);

	return buffer;
}

LPWSTR ListView::_getSize(const WIN32_FIND_DATA &fd)
{
	Drive drive;
	DWORD dwSize = fd.nFileSizeLow;

	return drive.ConvertByte(dwSize);
}