﻿// 1512397Lab04Explorer.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "1512397Lab04Explorer.h"
#include <windowsx.h>
#include "ListView.h"
#include "TreeView.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#define SPLITTER_WIDTH 4
#define TITLE_BAR 59
#define BLANK_BORDER 16
#define MAX_LOADSTRING 100

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512397LAB04EXPLORER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512397LAB04EXPLORER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICONEXPLORER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512397LAB04EXPLORER);
	wcex.lpszClassName = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICONEXPLORER));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

// Thông số của Window
int xWindow = 50;
int yWindow = 30;
int widthWindow;
int heightWindow;

ListView* g_Listview;
TreeView* g_Treeview;
Drive* g_Drive;
HWND addrFile;
HWND statusBar;
RECT g_TreeviewRect;
HFONT hFont;

// Thông số của Listview, Treeview
int top = 40; // y của Listview, Treeview
int height; // Listview, Treeview
int x_Tree = 8;
int x_List = 250;
int widthTree; 
int widthList;
int widthAdrr;

// Thông số chặn của splitter
int LeftMouse;
int RightMouse;
int AboveMouse;
int BelowMouse;

// INI
const int BUFFERSIZE = 260;
WCHAR buffer[BUFFERSIZE];
WCHAR curPath[BUFFERSIZE];
WCHAR configPath[BUFFERSIZE];

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   // Lấy thông số chiều rộng, cao của Window cha 
   GetCurrentDirectory(BUFFERSIZE, curPath);
   wsprintf(configPath, L"%s\\config.ini", curPath);
   GetPrivateProfileString(L"My Explorer", L"widthWin", L"965", buffer, BUFFERSIZE, configPath);
   widthWindow = _wtoi(buffer);
   GetPrivateProfileString(L"My Explorer", L"heightWin", L"700", buffer, BUFFERSIZE, configPath);
   heightWindow = _wtoi(buffer);
   if (widthWindow != 965)
	   widthWindow += BLANK_BORDER;
   if (heightWindow != 700)
	   heightWindow += TITLE_BAR;

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	   xWindow, yWindow, widthWindow, heightWindow, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	hFont = CreateFont(21, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);


	g_Listview = new ListView;
	g_Treeview = new TreeView;
	g_Drive = new Drive;
	g_Drive->_getSystemDir();


	int nStatusSize[3] = { 100, 300, -1 };
	statusBar = CreateWindowEx(0, STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, (HMENU)IDC_STATUSBAR, hInst, NULL);
	SendMessage(statusBar, SB_SETPARTS, 3, (LPARAM)&nStatusSize);

	g_Listview->Create(x_List, top, widthList, height, hWnd, ID_LV, hInst);
	g_Listview->List_LoadThisPc(g_Drive);

	// Lấy thông số độ rộng của Treeview
	GetPrivateProfileString(L"My Explorer", L"widthTree", L"238", buffer, BUFFERSIZE, configPath);
	widthTree = _wtoi(buffer);

	g_Treeview->Create(x_Tree, top, widthTree, height, hWnd, ID_TV, hInst);
	g_Treeview->Tree_LoadThisPC(g_Drive);
	g_Treeview->_getFocus();

	addrFile = CreateWindowEx(0, L"edit", L"This PC", WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER, x_Tree, 10, widthAdrr, 25, hWnd, (HMENU)ID_ADDR, hInst, NULL);
	SendMessage(addrFile, WM_SETFONT, WPARAM(hFont), TRUE);

	
	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm)
{
	LPNMTREEVIEW _pnmTree = (LPNMTREEVIEW)pnm;
	switch (pnm->code)
	{
	case TVN_ITEMEXPANDING:
		g_Treeview->Tree_LoadExpanding(_pnmTree->itemOld.hItem, _pnmTree->itemNew.hItem);
		break;
	case TVN_SELCHANGED:
		g_Listview->List_DeleteAll();
		g_Listview->List_LoadChild(g_Treeview->_getCurPath(), g_Drive);
		SetWindowText(addrFile, g_Treeview->_getCurPath());
		break;
	case NM_DBLCLK:
		if (pnm->hwndFrom == g_Listview->_getHandle())
			g_Listview->List_LoadFolderOrOpenFile(hWnd);
		break;
	case NM_CLICK:		
		if (pnm->hwndFrom == g_Listview->_getHandle()) {
			int count = 0;
			int flag = 0;
			DWORD sizeFile = 0;

			LPCWSTR path = g_Listview->_getPath(ListView_GetSelectionMark(g_Listview->_getHandle()));
			WIN32_FIND_DATA fd;
			GetFileAttributesEx(path, GetFileExInfoStandard, &fd);

			int iPos = ListView_GetNextItem(g_Listview->_getHandle(), -1, LVNI_SELECTED);
			while (iPos != -1) {
				count++;
				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
					sizeFile += fd.nFileSizeLow;
				else
					flag++;
				iPos = ListView_GetNextItem(g_Listview->_getHandle(), iPos, LVNI_SELECTED);
			}

			TCHAR *buffer = new TCHAR[34];
			if (count == 1)
				wsprintf(buffer, L"%d item selected", count);
			else
				wsprintf(buffer, L"%d items selected", count);

			if (count > 0) {
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)buffer);
			}
			else
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)L"");

			if(flag > 0)
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"");
			else
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)g_Drive->ConvertByte(sizeFile));
		}
		else {
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)L"");
			SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"");
		}
		break;
	}
	return 0;
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	// Lấy thông số rộng, cao của cửa sổ hiện hành
	widthWindow = cx;
	heightWindow = cy;

	// Lấy các thông số cần thiết để Resize
	if (widthTree > widthWindow)
		widthTree = widthWindow - 100;
	widthList = cx - widthTree - BLANK_BORDER - SPLITTER_WIDTH;
	widthAdrr = cx - BLANK_BORDER;
	height = cy - 63;

	// Resize cửa sổ
	MoveWindow(g_Treeview->_getHandle(), x_Tree, top, widthTree, height, TRUE);
	MoveWindow(g_Listview->_getHandle(), widthTree + BLANK_BORDER/2 + SPLITTER_WIDTH, top, widthList, height, TRUE);
	MoveWindow(addrFile, x_Tree, 10, widthAdrr, 25, TRUE);
	MoveWindow(statusBar, x_Tree, cy, cx, 25, TRUE);

	// Cập nhật lại thông số chặn của splitter
	LeftMouse = x_Tree + widthTree;
	RightMouse = LeftMouse + SPLITTER_WIDTH;
	AboveMouse = top;
	BelowMouse = top + height;
}

void OnLButtonDown(HWND hWnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	// Điều kiện set splitter
	if (x > LeftMouse && x < RightMouse && y > AboveMouse && y < BelowMouse) {
		SetCapture(hWnd);
		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE)));
	}
}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags) // x, y: Tọa độ chuột
{
	// Điều kiện set splitter
	if (x > LeftMouse && x < RightMouse && y > AboveMouse && y < BelowMouse)
		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE)));
	
	// Bắt sự kiện khi nhấn chuột trái
	if (keyFlags == MK_LBUTTON) {
		if (x > 60 && x < widthWindow - 100 && y > AboveMouse && y < BelowMouse) {
			widthTree = x - x_Tree;
			widthList = widthWindow - widthTree - BLANK_BORDER - SPLITTER_WIDTH;
			MoveWindow(g_Treeview->_getHandle(), x_Tree, top, widthTree, height, TRUE);
			MoveWindow(g_Listview->_getHandle(), x + SPLITTER_WIDTH, top, widthList, height, TRUE);
		}
	}
}

void OnLButtonUp(HWND hWnd, int x, int y, UINT keyFlags)
{
	ReleaseCapture();
	LeftMouse = x_Tree + widthTree;
	RightMouse = LeftMouse + SPLITTER_WIDTH;
	AboveMouse = top;
	BelowMouse = top + height;

}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	WCHAR widthTreeStr[10];

	wsprintf(widthTreeStr, L"%d", widthTree);
	WritePrivateProfileString(L"My Explorer", L"widthTree", widthTreeStr, configPath);

	WCHAR widthWinStr[10];
	wsprintf(widthWinStr, L"%d", widthWindow);
	WritePrivateProfileString(L"My Explorer", L"widthWin", widthWinStr, configPath);
	WCHAR heightWinStr[10];
	wsprintf(heightWinStr, L"%d", heightWindow);
	WritePrivateProfileString(L"My Explorer", L"heightWin", heightWinStr, configPath);

	PostQuitMessage(0);
}