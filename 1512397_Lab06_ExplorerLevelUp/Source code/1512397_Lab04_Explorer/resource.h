//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512397Lab04Explorer.rc
//
#define IDC_MYICON                      2
#define IDD_1512397LAB04EXPLORER_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512397LAB04EXPLORER        107
#define IDI_SMALL                       108
#define IDC_1512397LAB04EXPLORER        109
#define ID_LV                           110
#define ID_TV                           111
#define ID_ADDR                         112
#define IDI_THISPC                      113
#define IDC_STATUSBAR                   114
#define IDR_MAINFRAME                   128
#define IDI_ICONDRIVE                   129
#define IDI_ICONEXPLORER                130
#define IDI_ICONTHISPC                  131
#define IDI_ICONFOLDER                  132
#define IDI_ICONFILE                    133
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           115
#endif
#endif
