Expected results

1. Sinh viên có thể biết cách chạy Visual Studio
  - Nhấn Windows > gõ "Visual Studio" hoặc Run "devenv"
2. Một solution được tạo với tên: "HelloWorld".
  - Vào File > New > Project (Ctrl + Shift + N)
  - Visual C++ > Win32 > Win32 project
3. Có thể tạo thành công từ mã nguồn mẫu đã có
  - (Menu) Build > Build Solution (Ctrl + Shift + B)
4. Ứng dụng hiển thị 1 cửa sổ trống khi thực thi từ Visual Studio
  - Ctrl + F5: Thực thi không gỡ lỗi
5. Khi đóng, quá trình kết thúc hoàn toàn (không có dấu hiệu trong Task Manager)
  - File > Exit
  - Ctrl + Shift + Esc để mở Task Manager
6. Sinh viên có thể hiểu kết quả của quá trình được tạo ra bởi Visual Studio: tập tin exe, các tập tin trung gian
7. Sinh viên biết cách làm thế nào để làm sạch mã nguồn đối với các tập tin trung gian không cần thiết.
  - (Menu) Build > Clean Solution