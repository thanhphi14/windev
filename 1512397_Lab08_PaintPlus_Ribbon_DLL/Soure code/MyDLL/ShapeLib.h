#pragma once
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

#ifdef SHAPELIBRARY_EXPORTS
#define SHAPELIBRARY_API __declspec(dllexport) 
#else
#define SHAPELIBRARY_API __declspec(dllimport) 
#endif

namespace ShapeLibrary
{
	//************************************************************************************//
	class Shapes
	{
	protected:
		Point D_start;
		Point D_end;
		bool D_shift;
	public:
		SHAPELIBRARY_API Shapes();
		SHAPELIBRARY_API ~Shapes();

		SHAPELIBRARY_API virtual void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift) = 0;
		SHAPELIBRARY_API virtual void ReDraw(Graphics* graphics, Pen* pen) = 0;
		SHAPELIBRARY_API virtual Shapes* CreateShape() = 0;
	};
	//************************************************************************************//

	class Ellipses : public Shapes
	{
	public:
		SHAPELIBRARY_API Ellipses();
		SHAPELIBRARY_API ~Ellipses();
		SHAPELIBRARY_API void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
		SHAPELIBRARY_API void ReDraw(Graphics* graphics, Pen* pen);
		SHAPELIBRARY_API Shapes* CreateShape();
	};
	//************************************************************************************//

	class Lines : public Shapes
	{
	public:
		SHAPELIBRARY_API Lines();
		SHAPELIBRARY_API ~Lines();

		SHAPELIBRARY_API void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
		SHAPELIBRARY_API void ReDraw(Graphics* graphics, Pen* pen);
		SHAPELIBRARY_API Shapes* CreateShape();
	};
	//************************************************************************************//

	class Rectangles : public Shapes
	{
	public:
		SHAPELIBRARY_API Rectangles();
		SHAPELIBRARY_API ~Rectangles();

		SHAPELIBRARY_API void Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift);
		SHAPELIBRARY_API void ReDraw(Graphics* graphics, Pen* pen);
		SHAPELIBRARY_API Shapes* CreateShape();
	};
}

