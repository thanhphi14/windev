﻿#include "stdafx.h"
#include "ShapeLib.h"

namespace ShapeLibrary
{
	Shapes::Shapes(){}

	Shapes::~Shapes(){}
	//************************************************************************************//

	Ellipses::Ellipses(){}

	Ellipses::~Ellipses(){}

	void Ellipses::Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift)
	{
		// Lưu thông số vẽ lại
		D_start = start;
		D_end = end;
		D_shift = shift;
		int width = abs(end.X - start.X);
		int height = abs(end.Y - start.Y);

		if (shift) {
			width = height = min(width, height);
			if (start.X > end.X)
				start.X = start.X - width;
			if (start.Y > end.Y)
				start.Y = start.Y - width;
		}
		else {
			if (start.X > end.X) start.X = end.X;
			if (start.Y > end.Y) start.Y = end.Y;
		}

		graphics->DrawEllipse(pen, start.X, start.Y, width, height);
	}

	void Ellipses::ReDraw(Graphics* graphics, Pen* pen)
	{
		Draw(graphics, pen, D_start, D_end, D_shift);
	}

	Shapes* Ellipses::CreateShape()
	{
		Shapes *shape = new Ellipses;
		return shape;
	}
	//************************************************************************************//

	Lines::Lines(){}

	Lines::~Lines(){}

	void Lines::Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift)
	{
		// Lưu thông số vẽ lại
		D_start = start;
		D_end = end;
		D_shift = shift;

		graphics->DrawLine(pen, start, end);
	}

	void Lines::ReDraw(Graphics* graphics, Pen* pen)
	{
		Draw(graphics, pen, D_start, D_end, D_shift);
	}

	Shapes* Lines::CreateShape()
	{
		Shapes* shape = new Lines;
		return shape;
	}
	//************************************************************************************//

	Rectangles::Rectangles(){}

	Rectangles::~Rectangles(){}

	void Rectangles::Draw(Graphics* graphics, Pen* pen, Point start, Point end, bool shift)
	{
		// Lưu thông số vẽ lại
		D_start = start;
		D_end = end;
		D_shift = shift;

		int width = abs(end.X - start.X);
		int height = abs(end.Y - start.Y);

		if (shift) {
			width = height = min(width, height);
			if (start.X > end.X)
				start.X = start.X - width;
			if (start.Y > end.Y)
				start.Y = start.Y - width;
		}
		else {
			if (start.X > end.X) start.X = end.X;
			if (start.Y > end.Y) start.Y = end.Y;
		}

		graphics->DrawRectangle(pen, start.X, start.Y, width, height);
	}

	void Rectangles::ReDraw(Graphics* graphics, Pen* pen)
	{
		Draw(graphics, pen, D_start, D_end, D_shift);
	}

	Shapes* Rectangles::CreateShape()
	{
		Shapes *shape = new Rectangles;
		return shape;
	}
}