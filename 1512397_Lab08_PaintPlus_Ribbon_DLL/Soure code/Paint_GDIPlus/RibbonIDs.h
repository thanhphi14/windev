// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define cmdMenuGroupFile 2 
#define cmdMenuGroupFile_LabelTitle_RESID 60001
#define ID_BTN_NEW 3 
#define ID_BTN_NEW_LabelTitle_RESID 60002
#define ID_BTN_NEW_LargeImages_RESID 101
#define ID_BTN_ABOUT 4 
#define ID_BTN_ABOUT_LabelTitle_RESID 60003
#define ID_BTN_ABOUT_LargeImages_RESID 102
#define cmdMenuGroupExit 5 
#define cmdMenuGroupExit_LabelTitle_RESID 60004
#define ID_BTN_EXIT 6 
#define ID_BTN_EXIT_LabelTitle_RESID 60005
#define ID_BTN_EXIT_LargeImages_RESID 60006
#define cmdDraw 7 
#define cmdDraw_LabelTitle_RESID 60007
#define cmdShapes 8 
#define cmdShapes_LabelTitle_RESID 60008
#define ID_BTN_LINE 9 
#define ID_BTN_LINE_LabelTitle_RESID 60009
#define ID_BTN_LINE_LargeImages_RESID 201
#define ID_BTN_RECT 10 
#define ID_BTN_RECT_LabelTitle_RESID 60010
#define ID_BTN_RECT_LargeImages_RESID 202
#define ID_BTN_ELIP 11 
#define ID_BTN_ELIP_LabelTitle_RESID 60011
#define ID_BTN_ELIP_LargeImages_RESID 203
#define InternalCmd2_LabelTitle_RESID 60012
