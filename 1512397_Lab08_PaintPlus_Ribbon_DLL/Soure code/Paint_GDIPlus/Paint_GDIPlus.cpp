// 1512397SimplePaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Paint_GDIPlus.h"
#include "ShapeLib.h"
#include <windowsx.h>
#include <commctrl.h>
#include <vector>
#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"
#pragma comment(lib, "ComCtl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
using namespace std;
using namespace ShapeLibrary;

#define MAX_LOADSTRING 100
#define LINE 0
#define RECTANGLE 1
#define ELLIPSE 2
#define HEIGHTSTATUSBAR 23
#define LINE_WIDTH 2

// Lưu những hình đã vẽ
vector <Shapes*> FactoryShape;
// Kỹ thuật vẽ hàng mẫu
vector <Shapes*> PrototypeShape;

// Điểm bắt đầu, kết thúc của mỗi hình vẽ
Point g_start;
Point g_end;

// Các thông số kiểm tra khác
BOOL g_IsDrawingPreview = FALSE;
int IndexShapeType;
int LastShape;
BOOL flagShift = FALSE;

// GDI Plus
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
Graphics* g_graphics;
Pen* g_pen;

// Thanh Status bar 
HWND statusBar;

int widthWindow;
int heightWindow;
bool initSuccess;

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

												// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
		return FALSE;

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PAINTGDIPLUS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINTGDIPLUS));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CoUninitialize();
	return (int)msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PAINTGDIPLUS);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_PAINT));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		100, 50, 950, 650, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_KEYDOWN:
		if (wParam == VK_SHIFT) {
			flagShift = TRUE;
			if (IndexShapeType == 1)
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình vuông");
			if (IndexShapeType == 2)
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình tròn");
		}
		break;
	case WM_KEYUP:
		if (wParam == VK_SHIFT) {
			flagShift = FALSE;
			if (IndexShapeType == 1)
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình chữ nhật");
			if (IndexShapeType == 2)
				SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình ellipse");
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	//Tạo mới màn hình
	FactoryShape.clear();
	PrototypeShape.clear();
	InvalidateRect(hWnd, NULL, FALSE);

	// Bắt đầu Vẽ
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Khởi tạo Protopyte
	PrototypeShape.push_back(new Lines);
	PrototypeShape.push_back(new Rectangles);
	PrototypeShape.push_back(new Ellipses);

	// Tạo statusbar
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);
	int nStatusSize[3] = { 100, 220, -1 };
	statusBar = CreateWindowEx(0, STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, (HMENU)IDC_STATUSBAR, hInst, NULL);
	SendMessage(statusBar, SB_SETPARTS, 3, (LPARAM)&nStatusSize);

	WCHAR buffer[20];
	wsprintf(buffer, L"Tổng hình: 0");
	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)buffer);

	// Initializes the Ribbon framework.
	initSuccess = InitializeFramework(hWnd);
	if (!initSuccess)
		return -1;

	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case ID_SHAPES_LINE:
		g_IsDrawingPreview = FALSE;
		IndexShapeType = LINE;
		break;
	case ID_SHAPES_RECTANGLE:
		g_IsDrawingPreview = FALSE;
		IndexShapeType = RECTANGLE;
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình chữ nhật");
		break;
	case ID_SHAPES_ELLIPSE:
		g_IsDrawingPreview = FALSE;
		IndexShapeType = ELLIPSE;
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Hình ellipse");
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	// Thay đổi kích thước Status Bar
	MoveWindow(statusBar, 0, 0, cx, cy, TRUE);
}

void OnLButtonDown(HWND hWnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	//Nếu chưa bắt đầu vẽ thì tạo ra một hình mới
	if (!g_IsDrawingPreview)
		FactoryShape.push_back(PrototypeShape[IndexShapeType]->CreateShape());

	// Bắt đầu vẽ
	g_IsDrawingPreview = TRUE;
	g_start.X = x;
	g_start.Y = y;

	// Set đường biên
	SetCapture(hWnd);

	if (IndexShapeType == 0)
		SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"Đường thẳng");
}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags) // x, y: Tọa độ chuột
{
	if (g_IsDrawingPreview) {
		// Lấy thông số cửa sổ chính
		RECT rectWin;
		GetClientRect(hWnd, &rectWin);

		g_end.X = x;
		g_end.Y = y;

		// Thông số chặn cửa sổ Vẽ
		if (g_end.Y > rectWin.bottom - HEIGHTSTATUSBAR) g_end.Y = rectWin.bottom - HEIGHTSTATUSBAR - 1;
		if (g_end.Y < 55) g_end.Y = 56;
		if (g_end.X < 0) g_end.X = 1;
		if (g_end.X > rectWin.right) g_end.X = rectWin.right - 1;

		LastShape = FactoryShape.size() - 1;
		FactoryShape[LastShape]->Draw(g_graphics, g_pen, g_start, g_end, flagShift);

		InvalidateRect(hWnd, NULL, FALSE);
	}

	WCHAR buffer[20];
	wsprintf(buffer, L" %d, %dpx", x, y);
	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer);
}

void OnLButtonUp(HWND hWnd, int x, int y, UINT keyFlags)
{
	WCHAR buffer[20];
	wsprintf(buffer, L"Tổng hình: %d", LastShape + 1);
	SendMessage(GetDlgItem(hWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)buffer);

	// Hủy đang vẽ
	g_IsDrawingPreview = FALSE;
	ReleaseCapture();
	InvalidateRect(hWnd, NULL, FALSE);
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	RECT rect;
	GetClientRect(hWnd, &rect);
	HDC hMemDC;
	hMemDC = CreateCompatibleDC(hdc);
	// Chọn một đôi tượng bitmap để mở rộng vùng hiển thị cho hMemDC
	HBITMAP bitmap, oBitmap;
	bitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
	oBitmap = (HBITMAP)SelectObject(hMemDC, bitmap);
	// Vẽ lại nền hMemDC
	FillRect(hMemDC, &rect, HBRUSH(GetBkColor(hMemDC)));

	//Xuất hình ảnh ra hMemDC
	g_graphics = new Graphics(hMemDC);
	g_pen = new Pen(Color(255, 0, 100, 0), LINE_WIDTH);

	// Vẽ lại toàn bộ hình đã có
	for (int i = 0; i < FactoryShape.size(); ++i)
		FactoryShape[i]->ReDraw(g_graphics, g_pen);

	// Vẽ hình hiện tại
	if (g_IsDrawingPreview) {
		LastShape = FactoryShape.size() - 1;
		FactoryShape[LastShape]->Draw(g_graphics, g_pen, g_start, g_end, flagShift);
	}

	if (!BitBlt(hdc, 0, 0, rect.right, rect.bottom, hMemDC, 0, 0, SRCCOPY))
		MessageBox(hWnd, L"Failed to transfer bit block", L"Error", MB_OK);
	// Phục hồi lại bitmap cũ cho hMemDC
	SelectObject(hMemDC, oBitmap);
	// Giải phóng hMemDC, bitmap đã tạo
	DeleteDC(hMemDC);
	DeleteObject(bitmap);

	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	delete g_graphics;
	delete g_pen;

	GdiplusShutdown(gdiplusToken);

	//Tears down the Ribbon framework
	DestroyFramework();
	PostQuitMessage(0);
}