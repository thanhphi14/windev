** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Tạo TreeView bên trái, ListView bên phải
	- Duyệt được toàn bộ thư mục trong This PC cả bên TreeView và ListView 
	- ListView: 
		+ Đối với ổ đĩa: Hiển thị Tên, Loại, Tổng dung lượng, Dung lượng trống
		+ Đối với thư mục: Hiển thị Tên, Loại, Ngày chỉnh sửa
		+ Đối với tập tin: Hiển thị Tên, Ngày chỉnh sửa, Kích thước
		+ Cài đạt được icon cứng: Ổ đĩa, Thư mục, Tập tin
	- TreeView:
		+ Tạo được Root là This PC
		+ Cài đặt được icon cứng: This PC, Ổ đĩa, Thư mục.
	- Tạo thanh đường dẫn đến thư mục ở cả ListView va TreeView

** Mainflow:
	- TreeView:
		+ Khi chạy chương trình lên, node This PC được hiển thị dưới dạng thu gọn, 
		bấm vào sẽ sổ xuống danh sách các node con là ổ đĩa.

		+ Khi bấm vào các ổ đĩa ở tặng thái thu gọn sẽ sổ xuống danh sách các thư 
		mục và tập tin con, tương tự đối với thư mục.

		+ Khi bấm vào 1 thư mục bất kì hoặc ổ đĩa nào có con hoặc This PC thì bên 
		ListView sẽ tự động xuất hiện các thư mục, tập tin và ổ đĩa con tương ứng.

		+ Khi bấm vào bất kì thư mục nào thì sẽ xuất hiện đường dẫn tương ứng trên
		thanh đường dẫn.
	- ListView:
		+ Nếu bấm vào 1 thư mục, ổ đĩa thì sẽ xuất hiện các thư mục hoặc tập tin 
		con tương ứng

		+ Nếu bấm vào 1 tập tin thì sẽ chạy ứng dụng cho tập tin đó

		+ Khi bấm vào các thư mục bất kì thì sẽ xuất hiện đường dẫn tương ứng trên
		thanh đường dẫn.
		
** Additional flow:

** Link Repo: https://thanhphi14@bitbucket.org/thanhphi14/windev.git
	