﻿#include "stdafx.h"
#include "Drive.h"

Drive::Drive()
{
	_DriveName = NULL;
	_DisplayName = NULL;
	_TypeDiskIndex = NULL;
	_Count = 0;
}

Drive::~Drive()
{
	for (int i = 0; i < _Count; i++)
	{
		delete[] _DriveName[i];
		delete[] _DisplayName[i];
	}

	delete[] _DriveName;
	delete[] _DisplayName;
	delete[] _TypeDiskIndex;
	_Count = 0;
}

int Drive::_getCount()
{
	return _Count;
}

wchar_t* Drive::_getDriveName(const int &num)
{
	return _DriveName[num];
}

wchar_t* Drive::_getDisplayName(const int &num)
{
	return _DisplayName[num];
}

int Drive::_getDiskIndex(const int & num)
{
	return _TypeDiskIndex[num];
}

void Drive::CountDisk()
{
	TCHAR buffer[MAX_PATH];

	GetLogicalDriveStrings(MAX_PATH, buffer);

	// Đếm số lượng ổ đĩa 
	for (int i = 0; !((buffer[i] == 0) && (buffer[i + 1] == 0)); ++i)
		if (buffer[i] == 0)
			_Count++;
	_Count++;
}

void Drive::_getDrives()
{
	TCHAR buffer[MAX_PATH];
	GetLogicalDriveStrings(MAX_PATH, buffer);

	_DriveName = new wchar_t*[_Count];

	for (int i = 0; i < _Count; i++) 
		_DriveName[i] = new wchar_t[4];

	int k = 0;
	int j;
	for (int i = 0; i < _Count; i++) {
		j = 0;
		while (buffer[k] != 0) {
			_DriveName[i][j] = buffer[k];
			j++;
			k++;
		}
		_DriveName[i][j] = 0;
		k++;
	}
}

void Drive::_setDisplayName()
{
	TCHAR buffer[30];
	_DisplayName = new wchar_t*[_Count];
	_TypeDiskIndex = new int[_Count];
	
	int type;
	for (int i = 0; i < _Count; i++) {
		_DisplayName[i] = new wchar_t[35];
		type = GetDriveType(_DriveName[i]);
		buffer[0] = { 0 };
		if (type == DRIVE_FIXED) {
			_TypeDiskIndex[i] = DRIVE_FIXED;
			GetVolumeInformation(_DriveName[i], buffer, 31, NULL, NULL, NULL, NULL, 0);
			StrCpy(_DisplayName[i], buffer);
		}
		else if (type == DRIVE_CDROM) {
			_TypeDiskIndex[i] = DRIVE_CDROM;
			GetVolumeInformation(_DriveName[i], buffer, 31, NULL, NULL, NULL, NULL, 0);
			if (wcslen(buffer) == 0)
				StrCpy(_DisplayName[i], _T("CD Rom"));
			else
				StrCpy(_DisplayName[i], buffer);
		}
		StrCat(_DisplayName[i], _T(" ("));
		StrNCat(_DisplayName[i], _DriveName[i], 3);
		StrCat(_DisplayName[i], _T(")"));
	}
}

LPWSTR Drive::_getTypeDisk(const int & num)
{
	if (_getDiskIndex(num) == DRIVE_FIXED)
		return L"Local Disk";
	else
		return L"CD Drive";
}

void Drive::_getSystemDir()
{	
	CountDisk();
	_getDrives();
	_setDisplayName();
}

LPWSTR Drive::_getSize(const int & num)
{
	__int64 size;
	SHGetDiskFreeSpaceEx(_getDriveName(num), NULL, (PULARGE_INTEGER)&size, NULL);

	return ConvertByte(size);
}

LPWSTR Drive::_getFreeSize(const int & num)
{
	__int64 freeSize;
	GetDiskFreeSpaceEx(_getDriveName(num), NULL, NULL, (PULARGE_INTEGER)&freeSize);
	return ConvertByte(freeSize);
}
 
LPWSTR Drive::ConvertByte(__int64 size)
{
	int type = 0;
	while (size >= 1024) {
		size /= 1024;
		type++;
	}

	TCHAR *buffer = new TCHAR[11];
	_itow_s(size, buffer, 11, 10);

	switch (type)
	{
	case 0:
		StrCat(buffer, L" bytes");
		break;
	case 1:
		StrCat(buffer, L" KB");
		break;
	case 2:
		StrCat(buffer, L" MB");
		break;
	case 3:
		StrCat(buffer, L" GB");
		break;
	case 4:
		StrCat(buffer, L" TB");
		break;
	}

	return buffer;
}
