﻿#include "stdafx.h"
#include "TreeView.h"

#define MAX_PATH_LEN 10240

#define IDI_THISPC 0
#define IDI_DRIVE 1
#define IDI_FOLDER 2

TreeView::TreeView()
{
	_hTreeView = NULL;
}

TreeView::~TreeView()
{
	DestroyWindow(_hTreeView);
}

void TreeView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	_hTreeView = CreateWindowEx(0, WC_TREEVIEW, L"Tree View", WS_CHILD | WS_VISIBLE | WS_BORDER | TVS_HASLINES| TVS_HASBUTTONS | TVS_LINESATROOT
		, x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);

	HIMAGELIST Imagelist = ImageList_Create(16, 16, ILC_COLOR32 | ILC_MASK, 3, 3);
	HICON hIcon;
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONTHISPC));
	ImageList_AddIcon(Imagelist, hIcon);
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONDRIVE));
	ImageList_AddIcon(Imagelist, hIcon);
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONFOLDER));
	ImageList_AddIcon(Imagelist, hIcon);

	TreeView_SetImageList(_getHandle(), Imagelist, TVSIL_NORMAL);
}

HWND TreeView::_getHandle()
{
	return _hTreeView;
}

LPCWSTR TreeView::_getPath(HTREEITEM hItem)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(_hTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

HTREEITEM TreeView::_getCurSel()
{
	return TreeView_GetNextItem(_hTreeView, NULL, TVGN_CARET);
}

LPCWSTR TreeView::_getCurPath()
{
	return _getPath(_getCurSel());
}

void TreeView::_getFocus()
{
	SetFocus(_hTreeView);
}

HTREEITEM TreeView::_getThisPC()
{
	return TreeView_GetRoot(_hTreeView);
}

void TreeView::Tree_LoadThisPC(Drive *drive)
{
	TV_INSERTSTRUCT tview;
	tview.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	//This PC
	tview.hParent = NULL;
	tview.hInsertAfter = TVI_ROOT;
	tview.item.iImage = IDI_THISPC;
	tview.item.iSelectedImage = IDI_THISPC;
	tview.item.pszText = L"This PC";
	tview.item.lParam = (LPARAM)L"This PC";
	HTREEITEM ThisPc = TreeView_InsertItem(_hTreeView, &tview);

	for (int i = 0; i < drive->_getCount(); ++i)
	{
		tview.hParent = ThisPc;
		tview.item.pszText = drive->_getDisplayName(i);
		tview.item.lParam = (LPARAM) drive->_getDriveName(i);
		tview.item.iImage = IDI_DRIVE;
		tview.item.iSelectedImage = IDI_DRIVE;
		HTREEITEM Drive = TreeView_InsertItem(_hTreeView, &tview);

		if ((drive->_getDiskIndex(i) == DRIVE_FIXED))
		{
			tview.hParent = Drive;
			tview.item.pszText = L"LoadChild";
			tview.item.cChildren = 1;
			tview.item.lParam = (LPARAM) L"LoadChild";
			TreeView_InsertItem(_hTreeView, &tview);
		}
	}

	TreeView_SelectItem(_hTreeView, ThisPc);
}

void TreeView::Tree_LoadChild(HTREEITEM &hParent, LPCWSTR path)
{
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, path);

	StrCat(buffer, L"\\*");

	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.iImage = IDI_FOLDER;
	tvInsert.item.iSelectedImage = IDI_FOLDER;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);
	BOOL bFound = true;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	TCHAR * folderPath;
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, L".") != 0) && (StrCmp(fd.cFileName, L"..") != 0))
		{
			tvInsert.item.pszText = fd.cFileName;
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];

			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, L"\\");
			StrCat(folderPath, fd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;
			HTREEITEM hItem = TreeView_InsertItem(_hTreeView, &tvInsert);
			
			Tree_PreLoad(hItem);
		}

		bFound = FindNextFileW(hFile, &fd);
	}
}

void TreeView::Tree_LoadExpanding(HTREEITEM hPrev, HTREEITEM hCurSel)
{
	if (hCurSel == _getThisPC())
		return;

	HTREEITEM hCurSelChild = TreeView_GetChild(_hTreeView, hCurSel);

	if (!StrCmp(_getPath(hCurSelChild), L"LoadChild"))
	{
		TreeView_DeleteItem(_hTreeView, hCurSelChild);
		Tree_LoadChild(hCurSel, _getPath(hCurSel));
	}
}

void TreeView::Tree_PreLoad(HTREEITEM hItem)
{
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, _getPath(hItem));
	
	if (wcslen(buffer) == 3) //Nếu quét các ổ đĩa
	{
		if (StrStr(buffer, L"A:") || StrStr(buffer, L"B:")) //Đĩa mềm hổng làm 
			return;
	}
	else
		StrCat(buffer, L"\\");

	StrCat(buffer, L"*");

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);

	if (hFile == INVALID_HANDLE_VALUE)
		return;

	BOOL bFound = true;

	//Trong khi còn tìm thấy file hay thư mục
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& (StrCmp(fd.cFileName, L".") != 0) && (StrCmp(fd.cFileName, L"..") != 0))
		{
			TV_INSERTSTRUCT tvInsert;
			tvInsert.hParent = hItem;
			tvInsert.hInsertAfter = TVI_LAST;
			tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
			tvInsert.item.pszText = NULL;
			tvInsert.item.lParam = (LPARAM) L"LoadChild";
			TreeView_InsertItem(_hTreeView, &tvInsert);
			bFound = FALSE;
		}
		else
			bFound = FindNextFileW(hFile, &fd);
	}//while
}
