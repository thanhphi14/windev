﻿#pragma once
#include "Drive.h"

class TreeView
{
private:
	HWND _hTreeView;
public:
	TreeView();
	~TreeView();
	void Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst);

	HWND _getHandle(); // Lấy Handle treeview

	LPCWSTR _getPath(HTREEITEM hItem); // Lấy đường dẫn
	HTREEITEM _getCurSel(); // Lấy đường dẫn được chọn
	LPCWSTR _getCurPath(); // Lấy đường dẫn hiện tại
	void _getFocus(); // Focus
	HTREEITEM _getThisPC(); // Lấy thư mục gốc

	void Tree_LoadThisPC(Drive *drive); // Tải ThisPC 
	void Tree_LoadChild(HTREEITEM &hParent, LPCWSTR path); // Tải con
	void Tree_LoadExpanding(HTREEITEM hPrev, HTREEITEM hCurSel); // Tải khi mở
	void Tree_PreLoad(HTREEITEM hItem); // Duyệt lại
};

