﻿#include "stdafx.h"
#include "ListView.h"

#define IDI_DRIVE 0
#define IDI_FOLDER 1
#define IDI_FILE 2

ListView::ListView()
{
	_hListView = NULL;
}

ListView::~ListView()
{
	DestroyWindow(_hListView);
}

void ListView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	_hListView = CreateWindowEx(0, WC_LISTVIEWW, L"List View", WS_CHILD | WS_VISIBLE | WS_BORDER |LVS_REPORT | LVS_EX_GRIDLINES, x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);
	InitTempCols();

	HIMAGELIST Imagelist = ImageList_Create(16, 16, ILC_COLOR32 | ILC_MASK, 3, 0);
	HICON hIcon;
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONDRIVE));
	ImageList_AddIcon(Imagelist, hIcon);
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONFOLDER));
	ImageList_AddIcon(Imagelist, hIcon);
	hIcon = LoadIcon(hParentInst, MAKEINTRESOURCE(IDI_ICONFILE));
	ImageList_AddIcon(Imagelist, hIcon);

	ListView_SetImageList(_getHandle(), Imagelist, LVSIL_NORMAL | LVSIL_SMALL);
}

HWND ListView::_getHandle()
{
	return _hListView;
}

LPCWSTR ListView::_getPath(int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(_hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

void ListView::List_DeleteAll()
{
	ListView_DeleteAllItems(_hListView);
}

void ListView::InitTempCols()
{
	LVCOLUMN LvCol0;
	LvCol0.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol0.fmt = LVCFMT_LEFT;
	LvCol0.cx = 230;
	LvCol0.pszText = L"Tên";
	ListView_InsertColumn(_hListView, 0, &LvCol0);

	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_InsertColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_InsertColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_InsertColumn(_hListView, 3, &LvCol3);
}

void ListView::InitDriveCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_RIGHT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Tổng dung lượng";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 150;
	LvCol3.pszText = L"Dung lượng trống";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::InitFolderCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::List_LoadThisPc(Drive * _drive)
{
	InitDriveCols();
	LV_ITEM ListV;

	for (int i = 0; i < _drive->_getCount(); i++)
	{
		//Cột tên
		ListV.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		ListV.iItem = i;
		ListV.iSubItem = 0;
		ListV.pszText = _drive->_getDisplayName(i);
		ListV.iImage = IDI_DRIVE;
		ListV.lParam = (LPARAM)_drive->_getDriveName(i);
		ListView_InsertItem(_hListView, &ListV);

		//Các cột còn lại
		ListV.mask = LVIF_TEXT;

		//Cột Loại
		ListV.iSubItem = 1;
		ListV.pszText = _drive->_getTypeDisk(i);
		ListView_SetItem(_hListView, &ListV);

		//Cột Tổng Dung Lượng
		ListV.iSubItem = 2;
		if (_drive->_getDiskIndex(i) == DRIVE_FIXED)
			ListV.pszText = _drive->_getSize(i);
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);

		//Cột Dung Lượng Trống
		ListV.iSubItem = 3;
		if (_drive->_getDiskIndex(i) == DRIVE_FIXED)
			ListV.pszText = _drive->_getFreeSize(i);
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);
	}
}

void ListView::List_LoadChild(LPCWSTR path, Drive *drive)
{
	if (path == NULL)
		return;

	if (!StrCmp(path, L"This PC"))
		List_LoadThisPc(drive);
	else
		List_LoadFileandFolder(path);
}

void ListView::List_LoadFileandFolder(LPCWSTR path)
{
	InitFolderCols();
	List_DeleteAll();

	TCHAR buffer[10240];
	StrCpy(buffer, path);

	if (wcslen(path) == 3) //Nếu quét các ổ đĩa
		StrCat(buffer, L"*");
	else
		StrCat(buffer, L"\\*");

	WIN32_FIND_DATA fd;
	HANDLE hFile;
	BOOL bFound = true;
	LV_ITEM lv;

	wchar_t *folderPath;
	int nItemCount = 0;

	//Thư mục
	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(StrCmp(fd.cFileName, L".") != 0) && (StrCmp(fd.cFileName, L"..") != 0))
		{
			folderPath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(folderPath, path);

			if (wcslen(path) != 3)
				StrCat(folderPath, L"\\");

			StrCat(folderPath, fd.cFileName);

			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = IDI_FOLDER;
			lv.lParam = (LPARAM)folderPath;
			ListView_InsertItem(_hListView, &lv);

			//Cột Loại
			ListView_SetItemText(_hListView, nItemCount, 1, L"Thư mục");

			//Cột Ngày chỉnh sửa
			ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));
			++nItemCount;
		}

		bFound = FindNextFileW(hFile, &fd);
	}

	// Tập tin
	wchar_t *filePath;

	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			filePath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(filePath, path);

			if (wcslen(path) != 3)
				StrCat(filePath, _T("\\"));

			StrCat(filePath, fd.cFileName);

			//Cột Tên tập tin
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = IDI_FILE;
			lv.lParam = (LPARAM)filePath;
			ListView_InsertItem(_hListView, &lv);

			//Cột Loại
			ListView_SetItemText(_hListView, nItemCount, 1, L"Tập tin");

			//Cột Kích thước
			ListView_SetItemText(_hListView, nItemCount, 3, _getSize(fd));

			//Cột Ngày chỉnh sửa
			ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));
			++nItemCount;
		}
		bFound = FindNextFileW(hFile, &fd);
	}
}

void ListView::List_LoadFolderOrOpenFile(HWND parentWnd)
{
	LPCWSTR path = _getPath(ListView_GetSelectionMark(_hListView));

	WIN32_FIND_DATA fd;
	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);

	//Thư mục thì mở
	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		ListView_DeleteAllItems(_hListView);
		List_LoadFileandFolder(path);
		SetDlgItemText(parentWnd, ID_ADDR, path);
	}
	else //Tập tin thì chạy
		ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
}

LPWSTR ListView::_getDateModified(const FILETIME &ftLastWrite)
{
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	wchar_t *buffer = new wchar_t[20];
	wsprintf(buffer, L"%02d/%02d/%04d    %02d:%02d", stLocal.wDay, stLocal.wMonth, stLocal.wYear, stLocal.wHour, stLocal.wMinute);

	return buffer;
}

LPWSTR ListView::_getSize(const WIN32_FIND_DATA &fd)
{
	Drive drive;
	DWORD dwSize = fd.nFileSizeLow;

	return drive.ConvertByte(dwSize);
}