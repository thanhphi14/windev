#pragma once
#include <commctrl.h>
#include <Windows.h>
#include <shlwapi.h>
#include <shellapi.h>
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "ComCtl32.lib")

class Drive
{
private:
	int _Count;
	wchar_t **_DriveName;
	wchar_t **_DisplayName;
	int* _TypeDiskIndex;
public:
	Drive();
	~Drive();

	int _getCount();
	wchar_t* _getDriveName(const int &num);
	wchar_t* _getDisplayName(const int &num);
	int _getDiskIndex(const int &num);

	void CountDisk();
	void _getDrives();
	void _setDisplayName();

	void _getSystemDir();
	LPWSTR _getTypeDisk(const int &num);
	LPWSTR _getSize(const int &num);
	LPWSTR _getFreeSize(const int &num);
	
	LPWSTR ConvertByte(__int64 size);
};

