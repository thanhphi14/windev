﻿#include "stdafx.h"
#include "ListView.h"

ListView::ListView()
{
	_hListView = NULL;
}

ListView::~ListView()
{
	DestroyWindow(_hListView);
}

void ListView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	INITCOMMONCONTROLSEX icex;

	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	_hListView = CreateWindowEx(0, WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT,
			x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);
	ListView_SetExtendedListViewStyleEx(_hListView, NULL, LVS_EX_FULLROWSELECT| LVS_EX_GRIDLINES);
	InitTempCols();
}

HWND ListView::_getHandle()
{
	return _hListView;
}

void ListView::InitTempCols()
{
	LVCOLUMN LvCol0;
	LvCol0.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol0.fmt = LVCFMT_LEFT;
	LvCol0.cx = 100;
	LvCol0.pszText = L"Ngày tháng";
	ListView_InsertColumn(_hListView, 0, &LvCol0);

	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 120;
	LvCol1.pszText = L"Loại chi tiêu";
	ListView_InsertColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 200;
	LvCol2.pszText = L"Nội dung";
	ListView_InsertColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_LEFT;
	LvCol3.cx = 100;
	LvCol3.pszText = L"Số tiền";
	ListView_InsertColumn(_hListView, 3, &LvCol3);
}

void ListView::ReadFromDataBase(WCHAR* filename, HWND txtTotal)
{
	int totalMoney = 0;
	LV_ITEM ListV;
	std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);// write file as uft8 mode
	std::wifstream datain(filename);
	if (datain) {
		datain.imbue(loc);
		while (!datain.eof())
		{
			std::wstring bufDate;
			std::wstring bufType;
			std::wstring bufContent;
			std::wstring bufMoney;

			std::getline(datain, bufDate);
			std::getline(datain, bufType);
			std::getline(datain, bufContent);
			std::getline(datain, bufMoney);

			if (bufDate.length() == 0)
				return;

			ListV.mask = LVIF_TEXT;
			ListV.iItem = 0;
			ListV.iSubItem = 0;
			ListV.pszText = (LPWSTR)bufDate.c_str();
			ListView_InsertItem(_hListView, &ListV);

			ListV.iSubItem = 1;
			ListV.pszText = (LPWSTR)bufType.c_str();
			ListView_SetItem(_hListView, &ListV);

			ListV.iSubItem = 2;
			ListV.pszText = (LPWSTR)bufContent.c_str();
			ListView_SetItem(_hListView, &ListV);

			ListV.iSubItem = 3;
			ListV.pszText = (LPWSTR)bufMoney.c_str();
			ListView_SetItem(_hListView, &ListV);

			totalMoney += _wtoi(bufMoney.c_str());
		}
		datain.close();
	}

	WCHAR *Total = new WCHAR[20];
	wsprintf(Total, L"%d", totalMoney);
	wsprintf(Total + lstrlen(Total), L" VND");
	SetWindowText(txtTotal, Total);
}

void ListView::AddData(std::wstring date, std::wstring type, std::wstring content, std::wstring money)
{
	LV_ITEM ListV;
	ListV.mask = LVIF_TEXT;
	ListV.iItem = 0;
	ListV.iSubItem = 0;
	ListV.pszText = (LPWSTR)date.c_str();
	ListView_InsertItem(_hListView, &ListV);

	ListV.iSubItem = 1;
	ListV.pszText = (LPWSTR)type.c_str();
	ListView_SetItem(_hListView, &ListV);

	ListV.iSubItem = 2;
	ListV.pszText = (LPWSTR)content.c_str();
	ListView_SetItem(_hListView, &ListV);

	ListV.iSubItem = 3;
	ListV.pszText = (LPWSTR)money.c_str();
	ListView_SetItem(_hListView, &ListV);
}
