#pragma once
#include <iostream>
#include <fcntl.h>	//_O_WTEXT
#include <io.h>    //_setmode()
#include <string>
#include <locale>
#include <codecvt> //possible C++11?
#include <fstream>
#include <vector>
#include <commctrl.h>
#pragma comment(lib, "ComCtl32.lib")
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

class Chart
{
public:
	Chart();
	~Chart();

	void DrawNewPieChart(HDC hdc, float _anuongM, float _dichuyenM,
		float _nhacuaM, float _xecoM, float _nhuyeuM, float _dichvuM);
};

