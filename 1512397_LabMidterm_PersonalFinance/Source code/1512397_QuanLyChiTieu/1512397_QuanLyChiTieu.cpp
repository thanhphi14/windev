// 1512397_QuanLyChiTieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <windowsx.h>
#include "1512397_QuanLyChiTieu.h"
#include "ListView.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);

void SaveDataBase(HWND hWnd); // Lưu dữ liệu mới vào các vector và hiển thị thêm chi tiêu vừa thêm
void WriteToDataBase(WCHAR* filename); // Ghi dữ liệu vào file
void PushDataBase(WCHAR* filename); // Load tất cả dữ liệu trong file vào các vector
void TotalMoney(std::vector <std::wstring> _MONEY); // Tính tổng tiền
void DeleteDataByAll(); // Xóa dữ liệu trên ListView theo mục Tất cả
void DeleteDataByDate(std::wstring wdate);// Xóa dữ liệu trên ListView theo từng ngày
void UpdateInfoChart(std::vector <std::wstring> _TYPE, std::vector <std::wstring> _MONEY); // Cập nhật dữ liệu để vẽ biểu đồ
void GetStatisticalDataCBB(); // Lấy dữ liệu ban đầu vào combobox thống kê theo ngày
void AddStatisticalDataCBB(); // Thêm dữ liệu vào combobox thống kê
void DeleteStatisticalDataCBB(); // Xóa dữ liệu từ combobox thống kê
void SetStatisticalDataToListView(WCHAR* date); // Hiển thị dữ liệu thống kê theo ngày vào Listview
WCHAR* GetDataFromHandle(HWND cbbHwnd); // Lấy ngày thống kê
void SetStatisticalDataToCombobox(); // Gán dữ liệu thống kê vào comboboc để thêm đúng ngày.

bool IsEmpty(int length); // Kiểm tra cửa sổ Edit có rỗng không
bool IsValid(WCHAR* butMoney, int length); // Kiểm tra sô tiền nhập vào có hợp lệ không

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
ListView* g_Listview;
Chart* g_chart;
WCHAR* filename = L"DataBase.txt";
bool flagAdd = false;

float anuongM, dichuyenM, nhacuaM, xecoM, nhuyeuM, dichvuM;

std::vector <std::wstring> _DATE, _TYPE, _CONTENT, _MONEY;
std::vector <std::wstring> _SUBDATE, _SUBTYPE, _SUBCONTENT, _SUBMONEY;
std::vector <int> PosSubArr;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1512397QUANLYCHITIEU, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512397QUANLYCHITIEU));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MONEY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY1512397QUANLYCHITIEU);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MONEY));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX,
      180, 40, 910, 630, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

HWND sttTotal;
HWND DatePicker;
HWND cbbType, cbbStatistic;
HWND txtContent, txtMoney, txtTotal;
HWND btnAdd, btnDel;
HWND percentAU, percentDC, percentNC, percentXC, percentNYP, percentDV;
HWND AU, DC, NC, XC, NYP, DV;
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_CTLCOLORSTATIC:
		if (lParam == (long)txtTotal) {
			hdc = (HDC)wParam;
			SetTextColor(hdc, RGB(255, 0, 0));
		}
		SetBkColor((HDC)wParam, RGB(240, 240, 240));
		return (INT_PTR)CreateSolidBrush(RGB(240, 240, 240));
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Bắt đầu Vẽ
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(18, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, L"Segoe UI Semibold");

	HWND titleName1 = CreateWindowEx(0, L"Button", L"Thêm một loại chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 10, 875, 110, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(titleName1, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND titleDate = CreateWindowEx(0, L"Static", L"Ngày tháng:", WS_CHILD | WS_VISIBLE, 30, 50, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(titleDate, WM_SETFONT, WPARAM(hFont), TRUE);
	DatePicker = CreateWindowEx(0, DATETIMEPICK_CLASS, L"", WS_CHILD | WS_VISIBLE, 30, 70, 110, 25, hWnd, NULL, hInst, NULL);
	SendMessage(DatePicker, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND _type = CreateWindowEx(0, L"Static", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE, 173, 50, 100, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(_type, WM_SETFONT, WPARAM(hFont), TRUE);
	cbbType = CreateWindowEx(0, L"Combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 173, 70, 130, 30, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(cbbType, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND _Content = CreateWindowEx(0, L"Static", L"Nội dung:", WS_CHILD | WS_VISIBLE, 335, 50, 70, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(_Content, WM_SETFONT, WPARAM(hFont), TRUE);
	txtContent = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 335, 70, 230, 25, hWnd, (HMENU)IDE_NOIDUNG, hInst, NULL);
	SendMessage(txtContent, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND _Money = CreateWindowEx(0, L"Static", L"Số tiền (VND):", WS_CHILD | WS_VISIBLE, 595, 50, 100, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(_Money, WM_SETFONT, WPARAM(hFont), TRUE);
	txtMoney = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 595, 70, 140, 25, hWnd, (HMENU)IDE_TIEN, hInst, NULL);
	SendMessage(txtMoney, WM_SETFONT, WPARAM(hFont), TRUE);

	btnAdd = CreateWindowEx(0, L"Button", L"Thêm", WS_CHILD | WS_VISIBLE, 770, 69, 100, 27, hWnd, (HMENU)IDB_THEM, hInst, NULL);
	SendMessage(btnAdd, WM_SETFONT, WPARAM(hFont), TRUE);
	btnDel = CreateWindowEx(0, L"Button", L"Xóa", WS_CHILD | WS_VISIBLE, 471, 155, 80, 30, hWnd, (HMENU)IDB_XOA, hInst, NULL);
	SendMessage(btnDel, WM_SETFONT, WPARAM(hFont), TRUE);
	cbbStatistic = CreateWindowEx(0, L"Combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 30, 155, 110, 28, hWnd, (HMENU)IDC_STATISTIC, hInst, NULL);
	SendMessage(cbbStatistic, WM_SETFONT, WPARAM(hFont), TRUE);
	SendMessage(cbbStatistic, CB_ADDSTRING, 0, (LPARAM)L"Tất cả");
	ComboBox_SetCurSel(cbbStatistic, 0);

	HWND titleName2 = CreateWindowEx(0, L"Button", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 130, 560, 430, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(titleName2, WM_SETFONT, WPARAM(hFont), TRUE);

	HWND titleName3 = CreateWindowEx(0, L"Button", L"Thống kê", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 575, 130, 310, 430, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(titleName3, WM_SETFONT, WPARAM(hFont), TRUE);

	sttTotal = CreateWindowEx(0, L"Static", L"Tổng cộng:", WS_CHILD | WS_VISIBLE, 375, 525, 140, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(sttTotal, WM_SETFONT, WPARAM(hFont), TRUE);
	txtTotal = CreateWindowEx(0, L"Edit", L"0 VND", WS_CHILD | WS_VISIBLE | ES_READONLY, 450, 525, 115, 25, hWnd, (HMENU)IDE_TIEN, hInst, NULL);
	SendMessage(txtTotal, WM_SETFONT, WPARAM(hFont), TRUE);

	g_chart = new Chart;
	g_Listview = new ListView;
	g_Listview->Create(30, 190, 520, 320, hWnd, ID_LISTVIEW, hInst);
	SendMessage(g_Listview->_getHandle(), WM_SETFONT, WPARAM(hFont), TRUE);

	g_Listview->ReadFromDataBase(filename, txtTotal); // Đọc dữ liệu từ file để hiện lên cửa sổ
	PushDataBase(filename); // Ghi dữ liệu vào vector để sử dụng
	GetStatisticalDataCBB(); // Lấy dữ liệu để set Combobox thống kê theo ngày

	WCHAR* ArrType[] = { L"Ăn uống", L"Di chuyển", L"Nhà cửa", L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ" };
	HWND ArrHWNDtitle[] = { AU, DC, NC, XC, NYP, DV };
	int ArrX[] = { 640, 640, 640, 780, 780, 780 };
	int ArrY[] = { 390, 450, 510, 390, 450, 510 };

	for (int i = 0; i < 6; i++) {
		SendMessage(cbbType, CB_ADDSTRING, 0, (LPARAM)ArrType[i]);
		ArrHWNDtitle[i] = CreateWindowEx(0, L"Static", ArrType[i], WS_CHILD | WS_VISIBLE, ArrX[i], ArrY[i], 100, 20, hWnd, (HMENU)0, hInst, NULL);
		SendMessage(ArrHWNDtitle[i], WM_SETFONT, WPARAM(hFont), TRUE);
	}
	ComboBox_SetCurSel(cbbType, 0);

	percentAU = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 636, 410, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentAU, WM_SETFONT, WPARAM(hFont), TRUE);
	percentDC = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 636, 470, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentDC, WM_SETFONT, WPARAM(hFont), TRUE);
	percentNC = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 636, 530, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentNC, WM_SETFONT, WPARAM(hFont), TRUE);
	percentXC = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 776, 410, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentXC, WM_SETFONT, WPARAM(hFont), TRUE);
	percentNYP = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 776, 470, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentNYP, WM_SETFONT, WPARAM(hFont), TRUE);
	percentDV = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY, 776, 530, 90, 20, hWnd, (HMENU)0, hInst, NULL);
	SendMessage(percentDV, WM_SETFONT, WPARAM(hFont), TRUE);

	UpdateInfoChart(_TYPE, _MONEY); // Lấy dữ liệu vẽ biểu đồ

	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	WCHAR* date2 = GetDataFromHandle(cbbStatistic);
	std::wstring wdate2(date2);
	std::wstring wtemp(L"Tất cả");

	switch (id)
	{
	case IDB_THEM:
		flagAdd = true;
		AddStatisticalDataCBB(); // Thêm ngày mới (nếu có) vào combobox Thống kê 
		SaveDataBase(hWnd); // Lưu dữ liệu vào các vector đồng thời xuất ra Listview dữ liệu mới
		InvalidateRect(hWnd, NULL, TRUE); // Vẽ lại biểu đồ
		SetWindowText(txtContent, L"");
		SetWindowText(txtMoney, L"");
		break;
	case IDB_XOA:
		if (wdate2 == wtemp)
			DeleteDataByAll();
		else
			DeleteDataByDate(wdate2);
		DeleteStatisticalDataCBB();
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}

	if (codeNotify == CBN_SELCHANGE) {
		if (id == IDC_STATISTIC) {
			SetStatisticalDataToListView(GetDataFromHandle(cbbStatistic));
			InvalidateRect(hWnd, NULL, TRUE);
		}
	}
}

void OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	g_chart->DrawNewPieChart(hdc, anuongM, dichuyenM, nhacuaM, xecoM, nhuyeuM, dichvuM);

	Graphics *graphic = new Graphics(hdc);
	graphic->SetSmoothingMode(SmoothingModeAntiAlias);
	Color ArrColor[] = { Color(255, 0, 0), Color(0, 255, 0), Color(0, 0, 255), Color(255, 255, 0), Color(0, 255, 255), Color(135, 172, 66) };
	int ArrX[] = { 550, 550, 550, 690, 690, 690 };
	int ArrY[] = { 390, 450, 510, 390, 450, 510 };
	float startAngle = 315;
	float sweepAngle = 45;

	for (int i = 0; i < 6; i++) {
		SolidBrush blackBrush(ArrColor[i]);
		Rect ellipseRect(ArrX[i], ArrY[i], 80, 70);
		graphic->FillPie(&blackBrush, ellipseRect, startAngle, sweepAngle);
	}
	
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	WriteToDataBase(filename);

	GdiplusShutdown(gdiplusToken);

	delete g_Listview;
	delete g_chart;
	PostQuitMessage(0);
}

void SaveDataBase(HWND hWnd)
{
	bool errContent = false;
	bool errMoney = false;
	WCHAR* bufferERROR = new WCHAR[255];;

	int sizeDate = GetWindowTextLength(DatePicker);
	int sizeType = GetWindowTextLength(cbbType);
	int sizeContent = GetWindowTextLength(txtContent);
	int sizeMoney = GetWindowTextLength(txtMoney);

	if (IsEmpty(sizeContent)) {
		errContent = true;
		wsprintf(bufferERROR, L"Nội dung không đuộc rỗng !!!\n");
	}

	if (IsEmpty(sizeMoney)) {
		if (errContent)
			wsprintf(bufferERROR + lstrlen(bufferERROR), L"Số tiền phải là số nguyên dương !!!");
		else 
			wsprintf(bufferERROR, L"Số tiền phải là số nguyên dương !!!");
		errMoney = true;
	}

	WCHAR* butDate = GetDataFromHandle(DatePicker);
	WCHAR* butType = GetDataFromHandle(cbbType);
	WCHAR* butContent = GetDataFromHandle(txtContent);
	WCHAR* butMoney = GetDataFromHandle(txtMoney);
	
	if (!IsValid(butMoney, sizeMoney) && !errMoney) {
		if (errContent)
			wsprintf(bufferERROR + lstrlen(bufferERROR), L"Mệnh giá nhỏ nhất là 100 VND !!!");
		else
			wsprintf(bufferERROR, L"Mệnh giá nhỏ nhất là 100 VND !!!");
		errMoney = true;
	}

	if (errContent || errMoney) {
		MessageBox(hWnd, bufferERROR, L"Lỗi:", MB_OK);
		delete[] bufferERROR;
		delete[] butDate;
		delete[] butType;
		delete[] butContent;
		delete[] butMoney;
		return;
	}

	std::wstring wDate(butDate);
	std::wstring wType(butType);
	std::wstring wContent(butContent);
	std::wstring wMoney(butMoney);

	_DATE.push_back(wDate);
	_TYPE.push_back(wType);
	_CONTENT.push_back(wContent);
	_MONEY.push_back(wMoney);

	WCHAR* date = GetDataFromHandle(cbbStatistic);
	std::wstring wdate(date);
	std::wstring wtemp(L"Tất cả");
	if (wdate == wtemp) {
		g_Listview->AddData(wDate, wType, wContent, wMoney);
		TotalMoney(_MONEY); // In ra tổng tiền
		UpdateInfoChart(_TYPE, _MONEY); // Cập nhật thông tin vẽ biểu đồ
	}
	else {
		SetStatisticalDataToCombobox();
		WCHAR* date2 = GetDataFromHandle(cbbStatistic);
		SetStatisticalDataToListView(date2);
	}

	delete[] bufferERROR;
	delete[] butDate;
	delete[] butType;
	delete[] butContent;
	delete[] butMoney;
}

void WriteToDataBase(WCHAR* filename)
{
	if (_TYPE.size() > 0) {
		std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);// write file as uft8 mode
		std::wofstream dataout(filename);
		if (!dataout) {
			MessageBox(0, L"Không thể lưu dữ liệu", L"Lỗi", 0);
		}
		else {
			dataout.imbue(loc);
			for (int i = 0; i < _TYPE.size() - 1; i++) {
				dataout << _DATE[i] << L"\n" << _TYPE[i] << L"\n" << _CONTENT[i] << L"\n" << _MONEY[i] << L"\n";
			}
			dataout << _DATE[_TYPE.size() - 1] << L"\n" << _TYPE[_TYPE.size() - 1] << L"\n" << _CONTENT[_TYPE.size() - 1] << L"\n" << _MONEY[_TYPE.size() - 1];
			dataout.close();
		}
	}
	else {
		std::wofstream dataout(filename);
	}
}

void PushDataBase(WCHAR* filename)
{
	std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);// write file as uft8 mode
	std::wifstream datain(filename);
	if (datain) {
		datain.imbue(loc);
		while (!datain.eof())
		{
			std::wstring butDate;
			std::wstring butType;
			std::wstring butContent;
			std::wstring butMoney;

			std::getline(datain, butDate);
			std::getline(datain, butType);
			std::getline(datain, butContent);
			std::getline(datain, butMoney);

			if (butDate.length() == 0)
				break;

			_DATE.push_back(butDate);
			_TYPE.push_back(butType);
			_CONTENT.push_back(butContent);
			_MONEY.push_back(butMoney);
		}
		datain.close();
	}
}

void TotalMoney(std::vector <std::wstring> _MONEY)
{
	int totalMoney2 = 0;
	for (int i = 0; i < _MONEY.size(); i++) {
		totalMoney2 += _wtoi(_MONEY[i].c_str());
	}

	WCHAR *Total = new WCHAR[20]; 
	wsprintf(Total, L"%d", totalMoney2);
	wsprintf(Total + lstrlen(Total), L" VND");
	SetWindowText(txtTotal, Total);
}

void DeleteDataByAll()
{
	int iPos = ListView_GetNextItem(g_Listview->_getHandle(), -1, LVNI_SELECTED);
	std::vector<int>itemArr;
	while (iPos != -1) {
		itemArr.push_back(iPos);
		iPos = ListView_GetNextItem(g_Listview->_getHandle(), iPos, LVNI_SELECTED);
	}
	int numSel = itemArr.size();

	int itempDel;
	if (numSel > 0) {
		itempDel = itemArr[0];
		ListView_DeleteItem(g_Listview->_getHandle(), itempDel);
		_DATE.erase(_DATE.begin() + _DATE.size() - 1 - itempDel);
		_TYPE.erase(_TYPE.begin() + _TYPE.size() - 1 - itempDel);
		_CONTENT.erase(_CONTENT.begin() + _CONTENT.size() - 1 - itempDel);
		_MONEY.erase(_MONEY.begin() + _MONEY.size() - 1 - itempDel);
	}

	int tempPos = 1;
	for (int i = 0; i < numSel - 1; i++) {
		if (itemArr[i + 1] - itemArr[i] != 1) 
			itempDel = itemArr[i + 1] - tempPos;
		tempPos++;
		
		ListView_DeleteItem(g_Listview->_getHandle(), itempDel);
		_DATE.erase(_DATE.begin() + _DATE.size() - 1 - itempDel);
		_TYPE.erase(_TYPE.begin() + _TYPE.size() - 1 - itempDel);
		_CONTENT.erase(_CONTENT.begin() + _CONTENT.size() - 1 - itempDel);
		_MONEY.erase(_MONEY.begin() + _MONEY.size() - 1 - itempDel);
	}

	TotalMoney(_MONEY);
	UpdateInfoChart(_TYPE, _MONEY);
}

void DeleteDataByDate(std::wstring wdate)
{
	int iPos = ListView_GetNextItem(g_Listview->_getHandle(), -1, LVNI_SELECTED);
	std::vector<int>itemArr;
	while (iPos != -1) {
		itemArr.push_back(iPos);
		iPos = ListView_GetNextItem(g_Listview->_getHandle(), iPos, LVNI_SELECTED);
	}
	int numSel = itemArr.size();

	for (int i = 0; i < numSel; i++)
		ListView_DeleteItem(g_Listview->_getHandle(), itemArr[i] - i);

	for (int i = 0; i < numSel; i++) {
		_DATE.erase(_DATE.begin() + PosSubArr[PosSubArr.size() - 1 - itemArr[i]]);
		_TYPE.erase(_TYPE.begin() + PosSubArr[PosSubArr.size() - 1 - itemArr[i]]);
		_CONTENT.erase(_CONTENT.begin() + PosSubArr[PosSubArr.size() - 1 - itemArr[i]]);
		_MONEY.erase(_MONEY.begin() + PosSubArr[PosSubArr.size() - 1 - itemArr[i]]);
	}


	int totalMoney2 = 0;
	_SUBDATE.clear();
	_SUBTYPE.clear();
	_SUBCONTENT.clear();
	_SUBMONEY.clear();
	PosSubArr.clear();
	for (int i = 0; i < _DATE.size(); i++) {
		if (_DATE[i] == wdate) {
			totalMoney2 += _wtoi(_MONEY[i].c_str());
			_SUBTYPE.push_back(_TYPE[i]);
			_SUBMONEY.push_back(_MONEY[i]);
			PosSubArr.push_back(i);
		}
	}

	if (_SUBTYPE.size() == 0) {
		ComboBox_SetCurSel(cbbStatistic, 0);
		SetStatisticalDataToListView(GetDataFromHandle(cbbStatistic));
		return;
	}

	WCHAR *Total = new WCHAR[20];
	wsprintf(Total, L"%d", totalMoney2);
	wsprintf(Total + lstrlen(Total), L" VND");
	SetWindowText(txtTotal, Total);

	UpdateInfoChart(_SUBTYPE, _SUBMONEY);

}

void UpdateInfoChart(std::vector <std::wstring> _TYPE, std::vector <std::wstring> _MONEY)
{
	anuongM = 0; dichuyenM = 0; dichvuM = 0; nhacuaM = 0; xecoM = 0; nhuyeuM = 0;
	for (int i = 0; i < _TYPE.size(); i++) {
		if (_TYPE[i] == L"Ăn uống")
			anuongM += _wtoi(_MONEY[i].c_str());
		else if (_TYPE[i] == L"Di chuyển")
			dichuyenM += _wtoi(_MONEY[i].c_str());
		else if (_TYPE[i] == L"Nhà cửa")
			nhacuaM += _wtoi(_MONEY[i].c_str());
		else if (_TYPE[i] == L"Xe cộ")
			xecoM += _wtoi(_MONEY[i].c_str());
		else if (_TYPE[i] == L"Nhu yếu phẩm")
			nhuyeuM += _wtoi(_MONEY[i].c_str());
		else dichvuM += _wtoi(_MONEY[i].c_str());
	}

	float tongtien = anuongM + dichuyenM + nhacuaM + xecoM + nhuyeuM + dichvuM;

	if (tongtien > 0) {
		anuongM = anuongM / tongtien * 100;
		dichuyenM = dichuyenM / tongtien * 100;
		nhacuaM = nhacuaM / tongtien * 100;
		xecoM = xecoM / tongtien * 100;
		nhuyeuM = nhuyeuM / tongtien * 100;
		dichvuM = dichvuM / tongtien * 100;
	}

	std::wstring percent;
	float Arrfloat[] = { anuongM, dichuyenM, nhacuaM, xecoM, nhuyeuM, dichvuM };
	HWND ArrHwnd[] = { percentAU, percentDC, percentNC, percentXC, percentNYP, percentDV };
	for (int i = 0; i < 6; i++) {
		percent = std::to_wstring(Arrfloat[i]);
		percent = percent.substr(0, percent.length() - 4);
		WCHAR *Total = (WCHAR*)percent.c_str();
		wsprintf(Total + lstrlen(Total), L" %%");
		SetWindowText(ArrHwnd[i], Total);
	}
}

void GetStatisticalDataCBB()
{	
	std::vector<std::wstring> temp;
	if (_DATE.size() > 0) {
		temp.push_back(_DATE[0]);
		SendMessage(cbbStatistic, CB_ADDSTRING, 0, (LPARAM)temp[0].c_str());
		for (int i = 1; i < _DATE.size(); i++) {
			int flag = 0;
			for (int j = 0; j < temp.size(); j++) {
				if ((std::wstring)_DATE[i] != (std::wstring)temp[j])
					flag++;
			}
			if (flag == temp.size()) {
				SendMessage(cbbStatistic, CB_ADDSTRING, 0, (LPARAM)_DATE[i].c_str());
				temp.push_back(_DATE[i]);
			}
		}
	}
}

void AddStatisticalDataCBB()
{
	WCHAR* date = GetDataFromHandle(DatePicker);
	std::wstring wdate(date);
	int ncount = SendMessage(cbbStatistic, CB_GETCOUNT, 0, 0);
	int flag = 0;
	for (int i = 1; i < ncount; i++) {
		WCHAR* temp = new WCHAR[11];
		SendMessage(cbbStatistic, CB_GETLBTEXT, i, (LPARAM)temp);
		std::wstring wtemp(temp);
		if (wdate == wtemp) {
			flag = 1;
			break;
		}
	}

	if(flag == 0)
		SendMessage(cbbStatistic, CB_INSERTSTRING, -1, (LPARAM)date);
}

void DeleteStatisticalDataCBB()
{
	int k = SendMessage(cbbStatistic, CB_GETCURSEL, 0, 0);
	SendMessage(cbbStatistic, CB_RESETCONTENT, 0, 0);
	SendMessage(cbbStatistic, CB_ADDSTRING, 0, (LPARAM)L"Tất cả");

	GetStatisticalDataCBB();

	if (k == CB_ERR)
		ComboBox_SetCurSel(cbbStatistic, 0);
	else
		ComboBox_SetCurSel(cbbStatistic, k);
}

void SetStatisticalDataToListView(WCHAR* date)
{
	ListView_DeleteAllItems(g_Listview->_getHandle());		
	PosSubArr.clear();
	std::wstring wdate(date);
	std::wstring wtemp(L"Tất cả");
	if (wdate == wtemp) {
		for (int i = 0; i < _DATE.size(); i++)
			g_Listview->AddData(_DATE[i], _TYPE[i], _CONTENT[i], _MONEY[i]);
		if (!flagAdd)
			TotalMoney(_MONEY);
		UpdateInfoChart(_TYPE, _MONEY);
	}
	else {
		_SUBDATE.clear();
		_SUBTYPE.clear();
		_SUBCONTENT.clear();
		_SUBMONEY.clear();
		PosSubArr.clear();
		for (int i = 0; i < _DATE.size(); i++) {
			if (_DATE[i] == wdate) {
				if (!flagAdd)
					g_Listview->AddData(_DATE[i], _TYPE[i], _CONTENT[i], _MONEY[i]);
				_SUBDATE.push_back(_DATE[i]);
				_SUBTYPE.push_back(_TYPE[i]);
				_SUBCONTENT.push_back(_CONTENT[i]);
				_SUBMONEY.push_back(_MONEY[i]);
				PosSubArr.push_back(i);
			}
		}
		if (flagAdd) {
			for (int i = 0; i < _SUBDATE.size(); i++)
				g_Listview->AddData(_SUBDATE[i], _SUBTYPE[i], _SUBCONTENT[i], _SUBMONEY[i]);
			flagAdd = false;
		}
		TotalMoney(_SUBMONEY);
		UpdateInfoChart(_SUBTYPE, _SUBMONEY);
	}
}

WCHAR* GetDataFromHandle(HWND cbbHwnd)
{
	int size = GetWindowTextLength(cbbHwnd);
	WCHAR* date = new WCHAR[size + 1];
	GetWindowText(cbbHwnd, date, size + 1);
	return date;
}

void SetStatisticalDataToCombobox()
{
	WCHAR* date = GetDataFromHandle(DatePicker);
	std::wstring wdate(date);
	int ncount = SendMessage(cbbStatistic, CB_GETCOUNT, 0, 0);
	for (int i = 1; i < ncount; i++) {
		WCHAR* temp = new WCHAR[11];
		SendMessage(cbbStatistic, CB_GETLBTEXT, i, (LPARAM)temp);
		std::wstring wtemp(temp);
		if (wdate == wtemp) {
			ComboBox_SetCurSel(cbbStatistic, i);
			break;
		}
	}
}

bool IsEmpty(int length)
{
	if (length == 0)
		return true;
	return false;
}

bool IsValid(WCHAR * butMoney, int length)
{
	if (length < 3 || butMoney[length - 1] != '0' || butMoney[length - 2] != '0')
		return false;
	return true;
}