﻿#pragma once
#include "Chart.h"

class ListView
{
private:
	HWND _hListView;
public:
	ListView();
	~ListView();

	void Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst);
	HWND _getHandle(); // Lấy Handle Listview	

	void InitTempCols(); // Khởi tạo các cột
	void ReadFromDataBase(WCHAR* filename, HWND txtTotal);
	void AddData(std::wstring date, std::wstring type, std::wstring content, std::wstring money);
};

