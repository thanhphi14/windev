﻿#include "stdafx.h"
#include "Chart.h"

Chart::Chart()
{
}

Chart::~Chart()
{
}

void Chart::DrawNewPieChart(HDC hdc, float _anuongM, float _dichuyenM,
	float _nhacuaM, float _xecoM, float _nhuyeuM, float _dichvuM)
{
	Graphics *graphic = new Graphics(hdc);
	SolidBrush blackBrush1(Color(255, 0, 0));
	SolidBrush blackBrush2(Color(0, 255, 0));
	SolidBrush blackBrush3(Color(0, 0, 255));
	SolidBrush blackBrush4(Color(255, 255, 0));
	SolidBrush blackBrush5(Color(0, 255, 255));
	SolidBrush blackBrush6(Color(135, 172, 66));
	Pen pen(Color(0, 0, 0), 1);

	graphic->SetSmoothingMode(SmoothingModeHighQuality);

	if (_anuongM + _dichuyenM + _nhacuaM + _xecoM + _nhuyeuM + _dichvuM == 0)
	{
		graphic->DrawEllipse(&pen, 630, 160, 200, 200);
	}
	else {
		// Define the pie shape.
		Rect ellipseRect(630, 160, 200, 200);

		float startAngle = 270;
		float sweepAngle = _anuongM * 3.6;
		graphic->FillPie(&blackBrush1, ellipseRect, startAngle, sweepAngle);

		float startAngle2 = startAngle + sweepAngle;
		float sweepAngle2 = _dichuyenM * 3.6;
		graphic->FillPie(&blackBrush2, ellipseRect, startAngle2, sweepAngle2);

		float startAngle3 = startAngle2 + sweepAngle2;
		float sweepAngle3 = _nhacuaM * 3.6;
		graphic->FillPie(&blackBrush3, ellipseRect, startAngle3, sweepAngle3);

		float startAngle4 = startAngle3 + sweepAngle3;
		float sweepAngle4 = _xecoM * 3.6;
		graphic->FillPie(&blackBrush4, ellipseRect, startAngle4, sweepAngle4);

		float startAngle5 = startAngle4 + sweepAngle4;
		float sweepAngle5 = _nhuyeuM * 3.6;
		graphic->FillPie(&blackBrush5, ellipseRect, startAngle5, sweepAngle5);

		float startAngle6 = startAngle5 + sweepAngle5;
		float sweepAngle6 = _dichvuM * 3.6;
		graphic->FillPie(&blackBrush6, ellipseRect, startAngle6, sweepAngle6);
	}

	delete graphic;
}
