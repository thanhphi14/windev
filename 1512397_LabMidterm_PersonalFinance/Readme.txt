** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Thêm vào danh sách 1 chi tiêu, gồm 4 thành phần:
		+ NGÀY THÁNG
		+ Loại chi tiêu: Ăn uống, Di chuyển, Nhà cửa, Xe cộ, Nhu yếu phẩm, Dịch vụ
		+ Nội dung
		+ Số tiền
	- Các mục chi tiêu được lưu và nạp vào tập tin text
		+ Khi chương trình chạy lên, tự động nạp danh sách chi tiêu từ tập tin text 
		  lên và hiển thị trong Listview
		+ Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text
		+ Lưu các mục chi tiêu ở dang có dấu TIẾNG VIỆT
	- Pie Chart (biểu đồ tròn): Biểu diễn trực quan tỷ lệ tiêu xài
	- Làm thêm các chú thích phần trăm(%) cho biểu đồ tròn để dễ quan sát
	- Đã cãi tiến về mặt trải nghiệm người dùng
	- Làm thêm chức năng xem danh sách chi tiêu theo từng ngày:
		+ Người dùng có thể xem lại danh sách chi tiêu của mình ở những ngày trước đó
		+ Khi người dùng xem theo ngày thì Pie Chart tự động cập nhật danh sách chi tiêu 
		  của ngày đó để thể hiện 1 cách chính xác
		+ Tổng tiền cũng sẽ tự động tính toán để hiển thị theo từng ngày
	- Làm thêm chức năng xóa chi tiêu:
		+ Khi người dùng nhập sai nội dung hoặc không cần hiển thị 1 (nhiều) chi tiêu
		  thì có thể chọn các chi tiêu để xóa
		+ Biểu đồ sẽ tự động cập nhật
		+ Tỷ lệ % sẽ tự động cập nhật
		+ Tổng tiền sẽ tự động cập nhật
	
** Mainflow:
	- Khi chương trình chạy lên, tự động nạp danh sách chi tiêu từ tập tin text 
	  lên và hiển thị trong Listview
	- Thêm:
		+ Chọn ngày tháng
		+ Chọn Loại chi tiêu
		+ Nhập nội dung
		+ Nhập số tiền
		+ Nhấn nút THÊM
		=> Listview sẽ hiển thị các chi tiêu cũ và hiển thị thêm chi tiêu mới
		   Tổng tiền tự động cộng thêm chi tiêu mới vào
		   Biểu đồ tự động vẽ lại
		   Tỷ lệ phần trăm tự động cập nhật
	- Xem theo ngày:
		+ Chọn ngày tại combobox
		=> Listview sẽ hiển thị các chi tiêu của ngày đó
		   Tổng tiền tự động cập nhật lại chi tiêu của ngày đó
		   Biểu đồ tự động vẽ lại
		   Tỷ lệ phần trăm tự động cập nhật
	- Xóa:
		+ Chọn các chi tiêu muốn xóa trên Listview
		+ Chọn nút XÓA
		=> Listview sẽ xóa các chi tiêu được chọn
		   Tổng tiền tự động cập nhật lại
		   Biểu đồ tự động vẽ lại
		   Tỷ lệ phần trăm tự động cập nhật
	- Thoát:
		Nội dung mới tự động lưu lại vào tập tin text khi thoát chương trình
		
** Additional flow:	
	- Thêm:
		+ Không nhập nội dung: 
			Thông báo "Nội dung không đuộc rỗng !!!"
	        Không thêm
			Nhập lại
		+ Không nhập số tiền:
			Thông báo "Số tiền phải là số nguyên dương !!!"
			Không thêm
			Nhập lại
		+ Nhập số tiền bằng ký tự
			Chăn ngay, không cho nhập
			Không thêm
			Nhập lại
		+ Thêm khi đang ở chế độ xem theo ngày
			Nếu ngày thêm trùng ngày đang xem thì thêm vào
			Nếu khác ngày:
				Combobox thêm vào và hiển thị ngày mới
				Listview hiển thị nội dung vừa được thêm
			Tổng tiền, Biểu đồ, Phần trăm tự động cập nhật
	- Xóa:
		+ Xóa hết khi đang ở chế độ xem theo ngày:
			Tự động chuyển về chế độ xem Tất cả
			Xóa ngày đó trong combobox
			Listview hiển thị các chi tiêu còn lại (Tất cả)
			Tổng tiền, Biểu đồ, Phần trăm tự động cập nhật lại
					
** Link Youtube:
	https://youtu.be/Bx_3DYNnE-M
	
** Link Reponsitory:
	https://thanhphi14@bitbucket.org/thanhphi14/windev.git