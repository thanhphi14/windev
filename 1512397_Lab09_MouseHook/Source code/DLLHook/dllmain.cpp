// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include <Windowsx.h>

#define EXPORT  __declspec(dllexport)

HHOOK hHookKeyboard = NULL;
HHOOK hHookMouse = NULL;
HWND hWnd;
HINSTANCE hinstLib;
bool checkKey = false;

#define ID_DISABLE 110
#define ID_ENABLE 111

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);
extern "C" EXPORT INT BeginKeyboardHook(HWND hWnd);
extern "C" EXPORT INT EndKeyboardHook(HWND hWnd);
LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);
INT BeginMouseHook(HWND hWnd);
INT EndMouseHook(HWND hWnd);

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
		hinstLib = hModule;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }

    return TRUE;
}

HWND _hwnd = GetForegroundWindow();
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx(hHookKeyboard, nCode, wParam, lParam);
	KBDLLHOOKSTRUCT *kb = (KBDLLHOOKSTRUCT*)lParam;
	if (wParam == WM_KEYDOWN) {
		if (kb->vkCode == VK_SPACE) {
			int ctrl = GetKeyState(VK_LCONTROL);
			if (ctrl & 0x8000 && checkKey == false) {
				if (BeginMouseHook(hWnd)) {
					checkKey = true;
					SendMessage(_hwnd, WM_COMMAND, ID_ENABLE, 0);
				}
			}
			else if (ctrl & 0x8000 && checkKey == true) {
				if (EndMouseHook(hWnd)) {
					checkKey = false;
					SendMessage(_hwnd, WM_COMMAND, ID_DISABLE, 0);
				}
			}
		}
	}

	return CallNextHookEx(hHookKeyboard, nCode, wParam, lParam);
}

EXPORT INT BeginKeyboardHook(HWND hwnd)
{
	hWnd = hwnd;
	hHookKeyboard = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)KeyboardProc, hinstLib, 0);
	if (hHookKeyboard != NULL) {
		return 1;
	}
	return 0;
}

EXPORT INT EndKeyboardHook(HWND hwnd)
{
	hWnd = hwnd;
	if (hHookKeyboard != NULL) {
		UnhookWindowsHookEx(hHookKeyboard);
		hHookKeyboard = NULL;
		return 1;
	}
	return 0;
}

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		switch (wParam)
		{
		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK:
		case WM_LBUTTONUP:
			return 1;
		default:
			break;
		}
	}
	return CallNextHookEx(hHookMouse, nCode, wParam, lParam);
}

INT BeginMouseHook(HWND hWnd)
{
	hHookMouse = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)MouseProc, hinstLib, 0);
	if (hHookMouse != NULL) {
		return 1;
	}
	return 0;
}

INT EndMouseHook(HWND hWnd)
{
	if (hHookMouse != NULL) {
		UnhookWindowsHookEx(hHookMouse);
		hHookMouse = NULL;
		return 1;
	}
	return 0;
}
