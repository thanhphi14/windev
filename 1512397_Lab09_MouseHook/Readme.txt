** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Đưa các hàm xử lý Hook vào DLL
	- Vô hiệu hóa chuột trái khi bấm Ctrl + Space
	- Để kích hoạt lại chuột trái bấm Ctrl + Space
	
** Mainflow:
	- Chọn menu Hook -> InstallHook để bắt đầu sự kiện Hook
	- Bấm Ctrl + Space để vô hiệu hóa chuột trái
	- Bấm Ctrl + Space để kích hoạt lại chuột trái
	- Để Hủy sự hiện hook chọn menu Hook -> UninstallHook
		
** Additional flow:	
		
** Link Youtube:
	https://youtu.be/voQpmphVGNo
	
** Link Reponsitory:
	https://thanhphi14@bitbucket.org/thanhphi14/windev.git