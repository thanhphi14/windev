﻿#pragma once
#include "stdafx.h"

class ListView
{
private:
	HWND _hListView;
	HWND _hParent;
	wchar_t folderPath[10240];
public:

	ListView();
	~ListView();

	void Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst);

	HWND _getHandle(); // Lấy Handle Listview	
	LPCWSTR _getPath(int iItem); // Lấy đường dẫn
	LPWSTR _getSize(const WIN32_FIND_DATA &fd); // Lấy kích thước 
	LPWSTR _getDateModified(const FILETIME &ftLastWrite); // Lấy thời gian chỉnh sửa

	void InitTempCols(); // Khởi tạo các cột 
	void InitDriveCols(); // Khởi tạo cột cho Ổ đĩa
	void InitFolderCols(); // Khởi tạo cột cho file và folder

	void List_LoadThisPc(); // Tải ổ đĩa
	void List_LoadChild(LPCWSTR path); // Tải tất cả các con
	void List_LoadFileandFolder(LPCWSTR path); // Tải tập tin và thư mục
	void List_LoadFolderOrOpenFile(HWND parentWnd); // Tải thư mục hoặc mở file
	LPWSTR ConvertByte(__int64 size);
};

