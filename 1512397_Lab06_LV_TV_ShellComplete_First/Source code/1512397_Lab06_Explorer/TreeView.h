﻿#pragma once
#include "stdafx.h"

class TreeView
{
private:
	HWND _hTreeView;
public:
	TreeView();
	~TreeView();

	HWND _getHandle();
	LPCWSTR _getPath(HTREEITEM hItem);
	LPCWSTR _getCurPath();

	void Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst);

	HTREEITEM InsertThisPC(LPWSTR value, int image, int selectedImage, LPARAM lparam);
	void LoadDrive();
	HTREEITEM InsertChildren(LPWSTR value, HTREEITEM parent, int image, int selectedImage, LPARAM lparam, HTREEITEM InsetPos);

	void Tree_LoadThisPC(); // Tải ThisPC 

	HTREEITEM InsertFakeChild(HTREEITEM &parent);
	void DeleteAllChild(HTREEITEM parent);
	BOOL HasChild(LPCWSTR path);
	void Tree_LoadExpanding(TVITEM parent);
};