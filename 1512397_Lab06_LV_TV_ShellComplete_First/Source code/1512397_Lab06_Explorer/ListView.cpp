﻿#include "stdafx.h"
#include "ListView.h"


ListView::ListView()
{
	_hListView = NULL;
	_hParent = NULL;
}

ListView::~ListView()
{
	DestroyWindow(_hListView);
}

HWND ListView::_getHandle()
{
	return _hListView;
}

void ListView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	INITCOMMONCONTROLSEX icex;

	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	_hParent = parentWnd;
	_hListView = CreateWindowEx(0, WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_EX_GRIDLINES, x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);
	ListView_SetExtendedListViewStyleEx(_hListView, NULL, LVS_EX_FULLROWSELECT);
	InitTempCols();


	HIMAGELIST smallIcon;
	HIMAGELIST largeIcon;
	Shell_GetImageLists(&largeIcon, &smallIcon);

	ListView_SetImageList(_hListView, smallIcon, LVSIL_SMALL);
}

LPCWSTR ListView::_getPath(int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(_hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

LPWSTR ListView::_getSize(const WIN32_FIND_DATA &fd)
{
	DWORD dwSize = fd.nFileSizeLow;
	return ConvertByte(dwSize);
}

LPWSTR ListView::_getDateModified(const FILETIME &ftLastWrite)
{
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	wchar_t *buffer = new wchar_t[20];
	wsprintf(buffer, L"%02d/%02d/%04d   %02d:%02d", stLocal.wDay, stLocal.wMonth, stLocal.wYear, stLocal.wHour, stLocal.wMinute);

	return buffer;
}

void ListView::InitTempCols()
{
	LVCOLUMN LvCol0;
	LvCol0.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol0.fmt = LVCFMT_LEFT;
	LvCol0.cx = 230;
	LvCol0.pszText = L"Tên";
	ListView_InsertColumn(_hListView, 0, &LvCol0);

	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_InsertColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_InsertColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_InsertColumn(_hListView, 3, &LvCol3);
}

void ListView::InitDriveCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_RIGHT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Tổng dung lượng";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 150;
	LvCol3.pszText = L"Dung lượng trống";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::InitFolderCols()
{
	LVCOLUMN LvCol1;
	LvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol1.fmt = LVCFMT_LEFT;
	LvCol1.cx = 130;
	LvCol1.pszText = L"Loại";
	ListView_SetColumn(_hListView, 1, &LvCol1);

	LVCOLUMN LvCol2;
	LvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol2.fmt = LVCFMT_LEFT;
	LvCol2.cx = 150;
	LvCol2.pszText = L"Ngày chỉnh sửa";
	ListView_SetColumn(_hListView, 2, &LvCol2);

	LVCOLUMN LvCol3;
	LvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	LvCol3.fmt = LVCFMT_RIGHT;
	LvCol3.cx = 130;
	LvCol3.pszText = L"Kích thước";
	ListView_SetColumn(_hListView, 3, &LvCol3);
}

void ListView::List_LoadThisPc()
{
	InitDriveCols();
	LV_ITEM ListV;
	LPITEMIDLIST pidlItem = NULL;

	wchar_t buffer[MAX_PATH];
	GetLogicalDriveStrings(MAX_PATH, buffer);
	wchar_t* path = buffer;

	int k = 0;
	while (*path) {
		wchar_t* folderPath = new wchar_t[1024];
		WCHAR _typeDisk[35];
		SHFILEINFO sfi;
		pidlItem = ILCreateFromPath(path);
		SHGetFileInfo((LPCTSTR)pidlItem, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);

		StrCpy(folderPath, path);

		//Cột tên
		ListV.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		ListV.iItem = k;
		ListV.iSubItem = 0;
		ListV.pszText = sfi.szDisplayName;
		ListV.iImage = sfi.iIcon;
		ListV.lParam = (LPARAM)folderPath;
		ListView_InsertItem(_hListView, &ListV);

		//Các cột còn lại
		ListV.mask = LVIF_TEXT;

		//Cột Loại
		ListV.iSubItem = 1;
		ListV.pszText = sfi.szTypeName;
		ListView_SetItem(_hListView, &ListV);
		StrCpy(_typeDisk, sfi.szTypeName);
	
		//Cột Tổng Dung Lượng
		ListV.iSubItem = 2;
		if (StrCmp(_typeDisk, L"CD Drive")){
			__int64 size;
			SHGetDiskFreeSpaceEx(path, NULL, (PULARGE_INTEGER)&size, NULL);
			ListV.pszText = ConvertByte(size);
		}
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);

		//Cột Dung Lượng Trống
		
		ListV.iSubItem = 3;
		if (StrCmp(_typeDisk, L"CD Drive")){
			__int64 freeSize;
			GetDiskFreeSpaceEx(path, NULL, NULL, (PULARGE_INTEGER)&freeSize);
			ListV.pszText = ConvertByte(freeSize);
		}
		else
			ListV.pszText = NULL;
		ListView_SetItem(_hListView, &ListV);
		
		path = &path[lstrlen(path) + 1];
		k++;
	}
	
	TCHAR *buffer2 = new TCHAR[34];
	wsprintf(buffer2, L"%d items", k);
	SendMessage(GetDlgItem(_hParent, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer2);
}

void ListView::List_LoadFileandFolder(LPCWSTR path)
{
	if (path == NULL)
		return;

	InitFolderCols();
	ListView_DeleteAllItems(_hListView);
	LV_ITEM lv;
	int nItemCount = 0;
	TCHAR *folderPath;

	HRESULT hr;
	IShellFolder* pFolder = NULL;
	IEnumIDList* pEnumIds = NULL;
	LPITEMIDLIST pidl = NULL;
	LPITEMIDLIST pidlItem = NULL;
	WIN32_FIND_DATA fd;
	SHFILEINFO sfi;

	pidl = ILCreateFromPath(path);

	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)& pFolder);

	if (SUCCEEDED(hr))
	{
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds);
		if (SUCCEEDED(hr))
		{
			STRRET strDisplayName;
			while ((hr = pEnumIds->Next(1, &pidlItem, NULL)) == S_OK)
			{
				hr = pFolder->GetDisplayNameOf(pidlItem, SHGDN_NORMAL, &strDisplayName);
				SHGetDataFromIDList(pFolder, pidlItem, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
				SHGetFileInfo((LPCTSTR)pidlItem, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);
				if (SUCCEEDED(hr))
				{
					folderPath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
					StrCpy(folderPath, path);

					if (wcslen(path) != 3)
						StrCat(folderPath, L"\\");

					StrCat(folderPath, fd.cFileName);

					lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
					lv.iItem = nItemCount;
					lv.iSubItem = 0;
					lv.pszText = sfi.szDisplayName;
					lv.iImage = sfi.iIcon;
					lv.lParam = (LPARAM)folderPath;
					ListView_InsertItem(_hListView, &lv);

					//Cột Loại
					ListView_SetItemText(_hListView, nItemCount, 1, sfi.szTypeName);

					//Cột Ngày chỉnh sửa
					ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));
					++nItemCount;
				}
			}
		}


		hr = pFolder->EnumObjects(NULL, SHCONTF_NONFOLDERS, &pEnumIds);
		if (SUCCEEDED(hr))
		{
			STRRET strDisplayName;
			while ((hr = pEnumIds->Next(1, &pidlItem, NULL)) == S_OK)
			{
				hr = pFolder->GetDisplayNameOf(pidlItem, SHGDN_NORMAL, &strDisplayName);
				SHGetDataFromIDList(pFolder, pidlItem, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
				SHGetFileInfo((LPCTSTR)pidlItem, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);
				if (SUCCEEDED(hr))
				{
					folderPath = new wchar_t[wcslen(path) + wcslen(fd.cFileName) + 2];
					StrCpy(folderPath, path);

					if (wcslen(path) != 3)
						StrCat(folderPath, L"\\");

					StrCat(folderPath, fd.cFileName);

					lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
					lv.iItem = nItemCount;
					lv.iSubItem = 0;
					lv.pszText = sfi.szDisplayName;
					lv.iImage = sfi.iIcon;
					lv.lParam = (LPARAM)folderPath;
					ListView_InsertItem(_hListView, &lv);

					//Cột Loại
					ListView_SetItemText(_hListView, nItemCount, 1, sfi.szTypeName);

					//Cột Ngày chỉnh sửa
					ListView_SetItemText(_hListView, nItemCount, 2, _getDateModified(fd.ftLastWriteTime));

					//Cột Kích thước
					ListView_SetItemText(_hListView, nItemCount, 3, _getSize(fd));

					++nItemCount;
				}
			}
		}
	}

	TCHAR *buffer2 = new TCHAR[34];
	wsprintf(buffer2, L"%d items", nItemCount);
	SendMessage(GetDlgItem(_hParent, IDC_STATUSBAR), SB_SETTEXT, 0, (LPARAM)buffer2);
}

void ListView::List_LoadChild(LPCWSTR path)
{
	ListView_DeleteAllItems(_hListView);
	if (path == NULL)
		return;

	if (!StrCmp(path, L"This PC"))
		List_LoadThisPc();
	else
		List_LoadFileandFolder(path);
}

void ListView::List_LoadFolderOrOpenFile(HWND parentWnd)
{
	LPCWSTR path = _getPath(ListView_GetSelectionMark(_hListView));
	WIN32_FIND_DATA fd;

	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);

	//Thư mục thì mở
	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		ListView_DeleteAllItems(_hListView);
		SetDlgItemText(parentWnd, ID_ADDR, path);
		SendMessage(GetDlgItem(parentWnd, IDC_STATUSBAR), SB_SETTEXT, 1, (LPARAM)L"");
		SendMessage(GetDlgItem(parentWnd, IDC_STATUSBAR), SB_SETTEXT, 2, (LPARAM)L"");
		List_LoadFileandFolder(path);
	}
	else //Tập tin thì chạy
		ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
}

LPWSTR ListView::ConvertByte(__int64 size)
{
	int type = 0;
	__int64 right;

	while (size >= 1048576)
	{
		size /= 1024;
		++type;
	}

	if (size >= 1024)
	{
		//Lấy một chữ số sau thập phân của size chứa trong right
		right = size % 1024;

		while (right > 99)
			right /= 10;

		size /= 1024;
		++type;
	}
	else
		right = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(size, buffer, 11, 10);

	if (right != 0 && type > 1)
	{
		StrCat(buffer, L".");
		TCHAR *Right = new TCHAR[3];
		_itow_s(right, Right, 3, 10);
		StrCat(buffer, Right);
	}

	switch (type)
	{
	case 0:
		StrCat(buffer, L" bytes");
		break;
	case 1:
		StrCat(buffer, L" KB");
		break;
	case 2:
		StrCat(buffer, L" MB");
		break;
	case 3:
		StrCat(buffer, L" GB");
		break;
	case 4:
		StrCat(buffer, L" TB");
		break;
	}

	return buffer;
}