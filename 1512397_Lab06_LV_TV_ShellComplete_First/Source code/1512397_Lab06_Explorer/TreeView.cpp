﻿#include "stdafx.h"
#include "TreeView.h"

#define MAX_PATH_LEN 10240

TreeView::TreeView()
{
	_hTreeView = NULL;
}

TreeView::~TreeView()
{
	DestroyWindow(_hTreeView);
}

HWND TreeView::_getHandle()
{
	return _hTreeView;
}

LPCWSTR TreeView::_getPath(HTREEITEM hItem)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(_hTreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

LPCWSTR TreeView::_getCurPath()
{
	return _getPath(TreeView_GetNextItem(_hTreeView, NULL, TVGN_CARET));
}

void TreeView::Create(int x, int y, int width, int height, HWND parentWnd, long ID, HINSTANCE hParentInst)
{
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	_hTreeView = CreateWindowEx(0, WC_TREEVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_FULLROWSELECT
		, x, y, width, height, parentWnd, (HMENU)ID, hParentInst, NULL);

	HIMAGELIST smallIcon;
	HIMAGELIST largeIcon;
	Shell_GetImageLists(&largeIcon, &smallIcon);

	TreeView_SetImageList(_hTreeView, smallIcon, TVSIL_NORMAL);
}

HTREEITEM TreeView::InsertThisPC(LPWSTR value, int image, int selectedImage, LPARAM lparam)
{
	TV_INSERTSTRUCT tvInsert;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.cChildren = TRUE;
	tvInsert.item.iImage = image;
	tvInsert.item.iSelectedImage = selectedImage;
	tvInsert.item.lParam = lparam;
	tvInsert.item.pszText = value;
	return TreeView_InsertItem(_hTreeView, &tvInsert);
}

void TreeView::LoadDrive()
{
	HTREEITEM root = TreeView_GetRoot(_hTreeView);
	wchar_t* buffer = new wchar_t[1024];
	GetLogicalDriveStrings(MAX_PATH, buffer);
	
	int i = 0;
	while (buffer && lstrlen(buffer) > 1) {
		LPITEMIDLIST pidl;
		SHFILEINFO sfi;
		pidl = ILCreateFromPath(buffer);
		SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_ICON);
		HTREEITEM justInsert = InsertChildren(sfi.szDisplayName, root, sfi.iIcon, sfi.iIcon, (LPARAM)buffer, TVI_LAST);
		if (StrCmp(buffer, L"F:\\"))
			if (HasChild(buffer))
				InsertFakeChild(justInsert);
		buffer += lstrlen(buffer) + 1;
		i++;
	}
}

HTREEITEM TreeView::InsertChildren(LPWSTR value, HTREEITEM parent, int image, int selectedImage, LPARAM lparam, HTREEITEM InsetPos)
{
	TV_INSERTSTRUCT tvInsert;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.hParent = parent;
	tvInsert.hInsertAfter = InsetPos;
	tvInsert.item.cChildren = TRUE;
	tvInsert.item.iImage = image;
	tvInsert.item.iSelectedImage = selectedImage;
	tvInsert.item.lParam = lparam;
	tvInsert.item.pszText = value;
	return TreeView_InsertItem(_hTreeView, &tvInsert);
}

void TreeView::Tree_LoadThisPC()
{
	LPITEMIDLIST pidl;
	SHFILEINFO sfi;
	SHGetSpecialFolderLocation(NULL, CSIDL_DRIVES, &pidl);
	SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_ICON);
	HTREEITEM root = InsertThisPC(L"This PC", sfi.iIcon, sfi.iIcon, (LPARAM)L"This PC");

	LoadDrive();
	TreeView_Expand(_hTreeView, root, TVE_EXPAND);
	SetFocus(_hTreeView);
}

HTREEITEM TreeView::InsertFakeChild(HTREEITEM &parent)
{
	TV_INSERTSTRUCT dummy;
	dummy.item.mask = TVIF_TEXT;
	dummy.item.pszText = L"";
	dummy.hParent = parent;
	return TreeView_InsertItem(_hTreeView, &dummy);
}

void TreeView::DeleteAllChild(HTREEITEM parent)
{
	if (!parent) return;
	HTREEITEM item;
	while (item = TreeView_GetChild(_hTreeView, parent))
		TreeView_DeleteItem(_hTreeView, item);
}

BOOL TreeView::HasChild(LPCWSTR path)
{
	LPITEMIDLIST pidl = NULL;
	pidl = ILCreateFromPath(path);
	HRESULT hr = S_OK;
	IShellFolder* pFolder = NULL;
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void **)&pFolder);

	if (SUCCEEDED(hr))
	{
		LPITEMIDLIST pidlItems = NULL;
		IEnumIDList* pEnumIds = NULL;
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds); //lấy folder
		if ((hr = pEnumIds->Next(1, &pidlItems, NULL)) == S_OK)
		{
			pFolder->Release();
			return TRUE;
		}
	}
	pFolder->Release();
	return FALSE;
}

void TreeView::Tree_LoadExpanding(TVITEM parent)
{
	DeleteAllChild(parent.hItem);
	if (parent.hItem == TreeView_GetRoot(_hTreeView)) {
		LoadDrive();
		return;
	}

	wchar_t path[1024];
	StrCpy(path, (PWSTR)parent.lParam);

	LPITEMIDLIST pidl;
	pidl = ILCreateFromPath(path);
	HRESULT hr = S_OK;
	IShellFolder* pFolder = NULL;
	IEnumIDList* pEnumIds = NULL;
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void **)&pFolder);
	if (SUCCEEDED(hr))
	{
		LPITEMIDLIST pidlItems = NULL;
		SHFILEINFO sfi;
		STRRET str;
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds); //lấy folder
		while ((hr = pEnumIds->Next(1, &pidlItems, NULL)) == S_OK)
		{
			SHGetFileInfo((LPCTSTR)pidlItems, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);

			wchar_t* buffer = new wchar_t[1024];

			pFolder->GetDisplayNameOf(pidlItems, SHGDN_FORPARSING, &str);
			StrCpy(buffer, str.pOleStr);

			HTREEITEM hti = InsertChildren(sfi.szDisplayName, parent.hItem, sfi.iIcon, sfi.iIcon, (LPARAM)buffer, TVI_SORT);
			if (HasChild(buffer))
				InsertFakeChild(hti);
		}
	}
	pFolder->Release();
}