** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Tạo TreeView bên trái, ListView bên phải
	- Duyệt được toàn bộ thư mục trong This PC cả bên TreeView và ListView 
	- ListView: 
		+ Sử dụng Shell để duyệt thư mục
		+ Lấy được icon hệ thống cho tập tin, thư mục và ổ đĩa
		+ Lấy được loại tập tin hệ thống
	- TreeView:
		+ Tạo được Root là This PC
		+ Sử dụng Shell để duyệt thư mục
		+ Lấy được icon hệ thống cho thư mục, ổ đĩa và This PC
	- Tạo thanh đường dẫn đến thư mục ở cả ListView va TreeView
	- Tạo được Splitter
	- Tạo được tập tin config.ini
	- Tạo được Status Bar
** Mainflow:
	- Muốn chỉnh sửa kích thước 2 cửa sổ TreeView và ListView chỉ cần kéo thả trục giữa (Splitter)
	- Status bar
		+ Khi click vào 1 thư mục bên TreeView sẽ xuất hiện số lượng tập tin và thư mục con
		+ Khi chọn các Item bên ListView sẽ xuất hiện số lượng Item được chọn
		+ Khi chọn 1 tập tin sẽ hiển thị kích thước tập tin đó
	- Chỉnh sửa kích thước cửa sổ chính, khi tắt đi mở lại vẫn sẽ hiển thị cửa sổ giống như lúc vừa tắt
		
** Additional flow:	