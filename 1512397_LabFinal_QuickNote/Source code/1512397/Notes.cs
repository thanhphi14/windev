﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1512397
{
    public class Notes
    {
        public string NameNote { get; set; }
        public DateTime DateCreate { get; set; }
        public string Content { get; set; }
        public List<string> TagsOfNote = new List<string>();
    }
}
