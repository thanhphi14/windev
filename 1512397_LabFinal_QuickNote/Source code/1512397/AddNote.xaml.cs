﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _1512397
{
    /// <summary>
    /// Interaction logic for AddNote.xaml
    /// </summary>
    public partial class AddNote : Window
    {
        public AddNote()
        {
            InitializeComponent();
        }

        public Notes tempnote = new Notes();
        public string contentTag;   // Tạo 1 chuỗi lưu danh sách Tag
        private void btnAddNote_Click(object sender, RoutedEventArgs e)
        {
            if (tbNewTags.Text == "" || tbNameNewNote.Text == "")
            {
                MessageBox.Show("Tag hoặc tên note không được rỗng !!!");
            }
            else
            {
                // Lưu dữ liệu cửa sổ
                contentTag = tbNewTags.Text;
                tempnote.NameNote = tbNameNewNote.Text;
                tempnote.DateCreate = DateTime.Now;
                tempnote.Content = tbContentNewNote.Text.Trim();

                // Nếu số lượng Tag > 1 thì chia Tag ra thành nhiều Tag con và lưu vào tempnote.TagsOfNote
                if (contentTag.Trim().Length > 0)
                {
                    string[] token = contentTag.Split(new string[] { "," }, StringSplitOptions.None);
                    List<string> temptaglist = token.ToList<string>();
                    for (var i = 0; i < temptaglist.Count - 1; i++)
                    {
                        for (var j = i + 1; j < temptaglist.Count; j++)
                        {
                            if (temptaglist[i].Trim().ToLower() == temptaglist[j].Trim().ToLower())
                            {
                                temptaglist.RemoveAt(i);
                                i--;
                                break;
                            }
                        }
                    }

                    foreach (var temp in temptaglist)
                    {
                        tempnote.TagsOfNote.Add(temp.Trim());
                    }
                }
                DialogResult = true;
                this.Close();
            }
        }
    }
}
