﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _1512397
{
    /// <summary>
    /// Interaction logic for ContentNote.xaml
    /// </summary>
    public partial class ContentNote : Window
    {
        //static private Notes TempContentNote = new Notes();
        public ContentNote(Notes Note)
        {
            InitializeComponent();
            tbContentNoteName.Text = Note.NameNote;
            tbContentNote.Text = Note.Content;
            string TempTags = "";
            for (var i = 0; i < Note.TagsOfNote.Count; i++)
            {
                if (i == 0)
                {
                    TempTags = Note.TagsOfNote[i];
                }
                else
                {
                    TempTags += ", " + Note.TagsOfNote[i];
                }
            }
            tbTags.Text = TempTags;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Notes tempnote = new Notes();
        public string contentTag;   // Tạo 1 chuỗi lưu danh sách Tag
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (tbTags.Text == "" || tbContentNoteName.Text == "")
            {
                MessageBox.Show("Tag hoặc tên note không được rỗng !!!");
            }
            else
            {
                // Lưu dữ liệu cửa sổ
                contentTag = tbTags.Text;
                tempnote.NameNote = tbContentNoteName.Text;
                tempnote.DateCreate = DateTime.Now;
                tempnote.Content = tbContentNote.Text.Trim();

                // Nếu số lượng Tag > 1 thì chia Tag ra thành nhiều Tag con và lưu vào tempnote.TagsOfNote
                if (contentTag.Trim().Length > 0)
                {
                    string[] token = contentTag.Split(new string[] { "," }, StringSplitOptions.None);
                    List<string> temptaglist = token.ToList<string>();
                    for (var i = 0; i < temptaglist.Count - 1; i++)
                    {
                        for (var j = i + 1; j < temptaglist.Count; j++)
                        {
                            if (temptaglist[i].Trim().ToLower() == temptaglist[j].Trim().ToLower())
                            {
                                temptaglist.RemoveAt(i);
                                i--;
                                break;
                            }
                        }
                    }

                    foreach (var temp in temptaglist)
                    {
                        tempnote.TagsOfNote.Add(temp.Trim());
                    }
                }
                DialogResult = true;
                this.Close();
            }
        }
    }
}
