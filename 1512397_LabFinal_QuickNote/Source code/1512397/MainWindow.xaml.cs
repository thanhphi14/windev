﻿using System;
using Fluent;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace _1512397
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : RibbonWindow
    {
        // Khởi tạo cửa sổ
        public MainWindow()
        {
            InitializeComponent();
            System.Windows.Forms.NotifyIcon notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.Icon = new System.Drawing.Icon("Main.ico");
            notifyIcon.Visible = true;
            notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(notifyIcon_Click);

            System.Windows.Forms.ContextMenu notifyIconContextMenu = new System.Windows.Forms.ContextMenu();
            notifyIconContextMenu.MenuItems.Add("Exit", new EventHandler(Exit));
            notifyIconContextMenu.MenuItems.Add("View notes", new EventHandler(ViewNotes));
            notifyIconContextMenu.MenuItems.Add("View statistics", new EventHandler(ViewStatistics));

            notifyIcon.ContextMenu = notifyIconContextMenu;
        }

        // Hook
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private const int HOTKEY_ID = 9000;

        //Modifiers:
        private const uint MOD_NONE = 0x0000; //(none)
        private const uint MOD_ALT = 0x0001; //ALT
        private const uint MOD_CONTROL = 0x0002; //CTRL
        private const uint MOD_SHIFT = 0x0004; //SHIFT
        //private const uint MOD_WIN = 0x0008; //WINDOWS
        //SPACE:
        private const uint VK_SPACE = 0x20;

        private IntPtr _windowHandle;
        private HwndSource _source;
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            _windowHandle = new WindowInteropHelper(this).Handle;
            _source = HwndSource.FromHwnd(_windowHandle);
            _source.AddHook(HwndHook);

            RegisterHotKey(_windowHandle, HOTKEY_ID, MOD_CONTROL, VK_SPACE); //CTRL + SPACE
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            switch (msg)
            {
                case WM_HOTKEY:
                    switch (wParam.ToInt32())
                    {
                        case HOTKEY_ID:
                            int vkey = (((int)lParam >> 16) & 0xFFFF);
                            if (vkey == VK_SPACE)
                            {
                                AddNewNote();
                            }
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(HwndHook);
            UnregisterHotKey(_windowHandle, HOTKEY_ID);
            base.OnClosed(e);
        }

        public List<Tags> listTag = new List<Tags>();       // Danh sách các Tag
        public List<Notes> listNote = new List<Notes>();    // Danh sách các Note
        public List<int> listNoteIndex = new List<int>();   // Danh sách vị trí note
        private string filenameTag = @"FileTag.txt";
        private string filenameNote = @"FileNote.txt";
        Statistic statistic = new Statistic();

        // Xem thống kê trong Notifycation
        private void ViewStatistics(object sender, EventArgs e)
        {
            var statistics = new Statistic();
            statistics.Show();
        }

        // Xem note trong notifycation
        private void ViewNotes(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }

        // Thoát trong Notifycation
        private void Exit(object sender, EventArgs e)
        {
            SaveData();
            this.Close();
        }

        // Khi nhấp vào icon trong notifycation
        private void notifyIcon_Click(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if(e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if(this.WindowState == System.Windows.WindowState.Minimized)
                {
                    this.WindowState = System.Windows.WindowState.Normal;
                }
            }
        }

        // Lưu dữ liệu
        private void SaveData()
        {
            Stream myFile;
            // Nếu file chưa tồn tại thì tạo file
            if (!File.Exists(filenameTag))
            {
                myFile = File.Create(filenameTag);
                myFile.Close();
            }

            if (!File.Exists(filenameNote))
            {
                myFile = File.Create(filenameNote);
                myFile.Close();
            }

            // Ghi dữ liệu vào FileTag.txt
            using (StreamWriter streamWriter = new StreamWriter(filenameTag))
            {
                for (int i = 0; i < listTag.Count; i++)
                {
                    streamWriter.Write(listTag[i].Name);    // Ghi tên Tag
                    streamWriter.Write("\n");
                    streamWriter.Write(listTag[i].NumNote); // Ghi số lượng note
                    streamWriter.Write("\n");
                    streamWriter.Write(listTag[i].lNote);   // Ghi danh sách note
                    if (i < listTag.Count - 1)
                    {
                        streamWriter.Write("\n");
                    }
                }
            }

            // Ghi dữ liệu vào FileNote.txt
            using (StreamWriter streamWriter2 = new StreamWriter(filenameNote))
            {
                for (int i = 0; i < listNote.Count; i++)
                {
                    streamWriter2.Write(listNote[i].NameNote);      // Ghi tên note
                    streamWriter2.Write("\n");
                    streamWriter2.Write(listNote[i].DateCreate);    // Ghi ngày tạo note
                    streamWriter2.Write("\n");
                    streamWriter2.Write(listNote[i].Content);       // Ghi nội dung note
                    streamWriter2.Write("\n");
                    streamWriter2.Write("\n");

                    // Chuyển danh sách tag của note về 1 chuỗi để lưu
                    string tempTagList = "";
                    for (var k = 0; k < listNote[i].TagsOfNote.Count; k++)
                    {
                        if (k == 0)
                        {
                            tempTagList = listNote[i].TagsOfNote[k];
                        }
                        else
                        {
                            tempTagList += "," + listNote[i].TagsOfNote[k];
                        }
                    }
                    streamWriter2.Write(tempTagList);               // Ghi danh sách Tag
                    if (i < listNote.Count - 1)
                    {
                        streamWriter2.Write("\n");
                    }
                }
            }
        }

        // Hiển thị note dạng rút gọn
        private void Insert_lbNoteCollapse(int index, Notes note)
        {
            string str_CollapseContent;
            if (note.Content.Length > 50)
            {
                str_CollapseContent = note.Content.Substring(0, 49).Trim();
                str_CollapseContent += ".....";
            }
            else
            {
                str_CollapseContent = note.Content;
            }
            lbNote.Items.Insert(0, new Notes()    // Hiển thị từng note lên lbNote
            {
                NameNote = note.NameNote,
                DateCreate = note.DateCreate,
                Content = str_CollapseContent,
                TagsOfNote = note.TagsOfNote
            });
        }

        // Thêm tag mới
        public void AddNewTag(string tags, string namenote)
        {
            // Chia nhỏ danh sách tags từ khung nhập để lưu thành từng Tag riêng
            string[] tokenTag = tags.Split(new string[] { "," }, StringSplitOptions.None);
            List<string> temptaglist = tokenTag.ToList<string>(); // Chuyển Mảng string thanh List string

            // Xét từng phần tử của List, kiểm tra trùng không, nếu có, xóa phần tử trùng
            for(var i=0; i < temptaglist.Count - 1; i++)
            {
                for(var j=i+1; j < temptaglist.Count; j++)
                {
                    if(temptaglist[i].Trim().ToLower()== temptaglist[j].Trim().ToLower())
                    {
                        temptaglist.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }

            // Duyệt lầm lượt từng Tag mới với listTag đã có
            foreach(var tempTag in temptaglist)
            {
                bool flagEqual = false;
                for (var i=0; i < listTag.Count; i++)
                {
                    // Nếu trùng Tag thì 
                    if (tempTag.Trim().ToLower() == listTag[i].Name.ToLower()) 
                    {
                        listTag[i].NumNote += 1;                        // Tăng số lượng note của tag (NumNote) lên 1
                        listTag[i].lNote = listTag[i].lNote + "," + namenote;   // Thêm vào lNote của Tag 1 note mới
                        lbTag.Items.RemoveAt(i);                                // Xóa Tag tại vị trí trùng
                        lbTag.Items.Insert(i, listTag[i]);                      // Thêm vào lại lbTag Tag mới đã được cập nhật          
                        flagEqual = true;
                        break;
                    }
                }

                // Nếu không trùng, Thêm vào 1 Tag mới
                if (flagEqual == false) 
                {
                    listTag.Add(new Tags() { Name = tempTag.Trim(), NumNote = 1, lNote = namenote });
                    lbTag.Items.Add(listTag[listTag.Count - 1]);
                }
            }
        }

        // Hiển thị Note
        private void DisplayNote(int selectedIndexTag)
        {
            lbNote.Items.Clear();  // Xóa danh sách cũ
            listNoteIndex.Clear();
            // Nếu số lượng note của Tag > 1
            if (listTag[selectedIndexTag].NumNote > 1)
            {
                // Chia danh sách note thành từng note riêng
                string[] NotesOfTag = listTag[selectedIndexTag].lNote.Split(new string[] { "," }, StringSplitOptions.None);
                // Xóa đi những note trùng tên
                List<string> distinctNotesOfTag = NotesOfTag.ToList<string>().Distinct().ToList<string>();
                // Duyệt từng note vừa tách ra với listNote đã có
                foreach (var temp in distinctNotesOfTag)
                {
                    for (var i = 0; i < listNote.Count; i++)
                    {
                        // Nếu note mới trùng với 1 note cũ
                        if (temp == listNote[i].NameNote)
                        {
                            // So sánh Tag của note mới và Tag của note cũ
                            foreach (string strtemp in listNote[i].TagsOfNote)
                            {
                                // Nếu trùng thì xuất note lên lbNote
                                if (listTag[selectedIndexTag].Name.ToLower() == strtemp.ToLower())
                                {
                                    Insert_lbNoteCollapse(0, listNote[i]);
                                    listNoteIndex.Insert(0, i);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else // Nếu số lượng note của Tag <= 1
            {    // So sánh note đó với những note đã có (tương tự như trên)
                for (int i = 0; i < listNote.Count; i++)
                {
                    if (string.Compare(listTag[selectedIndexTag].lNote, listNote[i].NameNote, true) == 0)
                    {
                        foreach (string strtemp in listNote[i].TagsOfNote)
                        {
                            if (listTag[selectedIndexTag].Name.ToLower() == strtemp.ToLower())
                            {
                                Insert_lbNoteCollapse(0, listNote[i]);
                                listNoteIndex.Insert(0, i);
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Load dữ liệu
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Đọc những Tag từ FileTag.txt vào listTag
            if (File.Exists(filenameTag))
            {
                using (TextReader tr = new StreamReader(filenameTag))
                {
                    string line;
                    // Đọc từng Tag (3 dòng) kiểm tra file còn hay hết
                    while ((line = tr.ReadLine()) != null)
                    {
                        var tempTag = new Tags();
                        tempTag.Name = line;
                        tempTag.NumNote = Int32.Parse(tr.ReadLine());
                        tempTag.lNote = tr.ReadLine();
                        listTag.Add(tempTag);           // Thêm từng Tag vào listTag 
                        lbTag.Items.Add(tempTag);       // Hiển thị từng tag lên Tag listbox 
                    }
                }
            }

            // Đọc những Note từ FileNote.txt vào listNote
            if (File.Exists(filenameNote))
            {
                using (TextReader tr2 = new StreamReader(filenameNote))
                {
                    int NoteIndex = 0;
                    string line2;
                    // Đọc từng Note kiểm tra file còn hay hết
                    while ((line2 = tr2.ReadLine()) != null)
                    {
                        var tempNote = new Notes();
                        tempNote.NameNote = line2;                              // Đọc tên note
                        tempNote.DateCreate = DateTime.Parse(tr2.ReadLine());   // Đọc ngày giờ
                        // Đọc nội dung
                        string tempstr;
                        string contentstr = tr2.ReadLine(); // Đọc dòng nội dung đầu tiên
                        // Lặp cho đến khi gặp xuống dòng
                        while ((tempstr = tr2.ReadLine().Trim()) != "")
                        {
                            contentstr += "\n" + tempstr;   // Cộng thêm nội dung vào
                        }
                        tempNote.Content = contentstr;      // Gán nội dung note = nội dung vừa đọc được
                        
                        string tempTags = tr2.ReadLine();  // Đọc danh sách tag
                        // Chia nhỏ danh sách tag của note thành từng tag để lưu vào TagsOfNote của tempnote
                        string[] tokenTag = tempTags.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (var temp in tokenTag)
                        {
                            tempNote.TagsOfNote.Add(temp);
                        }
                        listNote.Add(tempNote);                // Thêm từng note vào listNote 
                        Insert_lbNoteCollapse(0, tempNote);      // Hiển thị từng note lên Note listbox
                        listNoteIndex.Insert(0, NoteIndex);
                        NoteIndex++;
                    }
                }
            }
        }

        // Chọn 1 Tag, hiển thị danh sách note
        private void SelectionListTag(object sender, SelectionChangedEventArgs e)
        {
            // Lấy vị trí lbTag được chọn
            int indexTag = lbTag.SelectedIndex;
            // Nếu > -1
            if (indexTag > -1)
            {
                DisplayNote(indexTag); // Hiển thị danh sách note của Tag
            }
        }

        // Chọn 1 Note và hiển thị, update
        private void SelectionListNote(object sender, SelectionChangedEventArgs e)
        {
            // Lấy vị trí lbNote được chọn
            var indexNote = lbNote.SelectedIndex;
            // Nếu > -1
            if (indexNote > -1)
            {
                // Hiển thị của sổ mới với nội dung của note đó
                ContentNote contentNote = new ContentNote(listNote[listNoteIndex[indexNote]]);

                string tempNameNote = listNote[listNoteIndex[indexNote]].NameNote;
                if (contentNote.ShowDialog() == true)
                {
                    // Nếu đổi tên Note
                    if(tempNameNote != contentNote.tempnote.NameNote)
                    {
                        for (var k = 0; k < listTag.Count; k++)
                        {
                            string[] NotesOfTag = listTag[k].lNote.Split(new string[] { "," }, StringSplitOptions.None);
                            //Xóa đi những note trùng tên
                            List<string> distinctNotesOfTag = NotesOfTag.ToList<string>().Distinct().ToList<string>();
                            listTag[k].lNote = "";
                            for (var i = 0; i < distinctNotesOfTag.Count; i++)
                            {
                                if (distinctNotesOfTag[i] != tempNameNote)
                                    listTag[k].lNote += distinctNotesOfTag[i];
                                else
                                    listTag[k].lNote += contentNote.tempnote.NameNote;

                                if (i < distinctNotesOfTag.Count - 1)
                                    listTag[k].lNote += ",";
                            }
                        }
                    }
                    // Nếu có thêm Tag mới
                    foreach (var temptag in contentNote.tempnote.TagsOfNote)
                    {
                        bool flagEqual = false;
                        foreach(var temptag2 in listNote[listNoteIndex[indexNote]].TagsOfNote)
                        {
                            // Nếu tag cũ = tag mới thì bỏ qua
                            if (temptag.ToLower() == temptag2.ToLower())
                            {
                                flagEqual = true;
                                break;
                            }
                        }

                        // Nếu không bằng
                        if (flagEqual == false)
                        {
                            bool flagExists = false;
                            for(var i=0; i < listTag.Count; i++)
                            {
                                // Nếu tồn tại rồi thì tăng số lượng note lên 1
                                if(temptag.ToLower() == listTag[i].Name.ToLower())
                                {
                                    listTag[i].NumNote += 1;
                                    listTag[i].lNote += "," + contentNote.tempnote.NameNote;
                                    lbTag.Items.RemoveAt(i);
                                    lbTag.Items.Insert(i, listTag[i]);
                                    flagExists = true;
                                    break;
                                }
                            }

                            // Nếu chưa tồn tại thì thêm Tag mới
                            if(flagExists == false)
                            {
                                listTag.Add(new Tags() { Name = temptag.Trim(), NumNote = 1, lNote = contentNote.tempnote.NameNote });
                                lbTag.Items.Add(listTag[listTag.Count - 1]);
                            }
                        }
                    }

                    // Nếu xóa Tag cũ
                    foreach(var temptag in listNote[listNoteIndex[indexNote]].TagsOfNote)
                    {
                        bool flagEqual = false;
                        foreach (var temptag2 in contentNote.tempnote.TagsOfNote)
                        {
                            // Nếu tag cũ = tag mới thì bỏ qua
                            if(temptag.ToLower() == temptag2.ToLower())
                            {
                                flagEqual = true;
                                break;
                            }
                        }

                        // Nếu không bằng
                        if(flagEqual == false)
                        {
                            for(var i=0; i < listTag.Count; i++)
                            {
                                // Xóa Note ra khỏi tag chứa
                                if(temptag.ToLower() == listTag[i].Name.ToLower())
                                {
                                    listTag[i].NumNote -= 1;
                                    string[] tokenNote = listTag[i].lNote.Split(new string[] { "," }, StringSplitOptions.None);
                                    List<string> listNotesOfTag = tokenNote.ToList<string>();
                                    listTag[i].lNote = "";
                                    int flag = 0;
                                    for (var j=0; j < listNotesOfTag.Count; j++)
                                    {
                                        if (listNotesOfTag[j] != contentNote.tempnote.NameNote)
                                        {
                                            listTag[i].lNote += listNotesOfTag[j];
                                            if (flag < listTag[i].NumNote - 1)
                                            {
                                                listTag[i].lNote += ",";
                                                flag++;
                                            }                                          
                                        }
                                    }

                                    // Cập nhật lại listTag
                                    lbTag.Items.RemoveAt(i);
                                    if(listTag[i].NumNote == 0)
                                    {
                                        listTag.RemoveAt(i);
                                    }
                                    else
                                    {
                                        lbTag.Items.Insert(i, listTag[i]);
                                    }
                                }
                            }
                        }
                    }

                    Insert_lbNoteCollapse(indexNote, contentNote.tempnote);
                    lbNote.Items.RemoveAt(indexNote + 1);
                    listNote[listNoteIndex[indexNote]] = contentNote.tempnote;              
                }
            }
            
            lbNote.UnselectAll();  // Bỏ chọn tất cả
        }

        // Thêm 1 note mới
        private void AddNewNote()
        {
            // Tạo cửa sổ thêm note
            var addNote = new AddNote();
            var indexTag = lbTag.SelectedIndex; // Lấy giá trị lbTag được chọn (nếu có)
            // Hiển thị cửa sổ mới và nhập nội dung vào
            if (addNote.ShowDialog().Value == true)
            {
                // Nếu lbNote hiện tại hiển thị đủ note thì hiển thị thêm note mới
                if (lbNote.Items.Count == listNote.Count)
                {
                    Insert_lbNoteCollapse(0, addNote.tempnote);
                    listNoteIndex.Insert(0, lbNote.Items.Count - 1);        // Thêm thứ tự note vào listNoteIndex
                }
                AddNewTag(addNote.contentTag, addNote.tempnote.NameNote);   // Thêm 1 Tag vào danh sách
                listNote.Add(addNote.tempnote);                             // Thêm 1 Note vào danh sách
                lbTag.SelectedIndex = indexTag;                             // Hiển thị note mới nếu có cùng tag
            }
        }
        
        // Nút thêm 1 note mới
        private void btnAddNote_Click(object sender, RoutedEventArgs e)
        {
            AddNewNote();
        }

        // Hiển thị tất cả note
        private void btnAllNote_Click(object sender, RoutedEventArgs e)
        {
            lbNote.Items.Clear();                       // Xóa toàn bộ items của lbNote
            listNoteIndex.Clear();                      // Xóa toàn bộ danh sách của listNoteIndex
            lbTag.SelectedIndex = -1;                   // Hủy chọn lbTag
            for (var i = 0; i < listNote.Count; i++)
            {
                Insert_lbNoteCollapse(0, listNote[i]);
                listNoteIndex.Insert(0, i);             
            }
        }

        // Đóng cửa sổ
        private void CloseWindow(object sender, EventArgs e)
        {
            SaveData();
        }

        // Đóng cửa sổ Ribbon -> File -> Close
        private void CloseHeaderFile(object sender, RoutedEventArgs e)
        {
            SaveData();
            this.Close();
        }

        // Hiển thị thống kê
        private void btnStatisticTag_Click(object sender, RoutedEventArgs e)
        {
            var statistic = new Statistic();
            statistic.Show();
        }
    }
}