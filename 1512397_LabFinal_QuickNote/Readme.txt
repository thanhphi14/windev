** My info:
	- Student ID: 1512397
	- Name: Ngô Thanh Phi
	- Email: thanhphi.dd@gmail.com

** I have done:
	- Thêm 1 Note vào listbox Note
		+ Tag
		+ Tên Note
		+ Nội dung
	- Hiển thị danh sách Tag
		+ Tên tag
		+ Số lượng note chưa tag
	- Hiển thị danh sách Note
		+ Tên note
		+ Ngày giờ tạo
		+ Nội dung note, tối đa 50 kí tự
	- Bấm vào 1 tag hiển thị các note chứa Tag
	- Bấm vào 1 note
		+ Danh sách tag
		+ Tên note
		+ Nội dung đầy đủ của note
	- Thêm được icon vào vùng notifycation
		+ Exit: Thoát App
		+ Viewnotes: Hiển thị cửa sổ listTag và listNote
		+ Viewstatistic: hiển thị cửa sổ thống kê Tag
	- Tạo được Ribbon với các nút
		+ Thêm note (Add note)
		+ Hiển thị tất cả note (All note)
		+ Hiển thị cửa sổ Thống kê (Statistic Tag)
	- Tạo được Hook: Ctrl + Space: Hiển thị cửa sổ thêm Note
	
** Mainflow:
	- Khi chương trình chạy lên, tự động nạp danh sách chi tiêu từ tập tin text 
	  lên và hiển thị trong ListTag, ListNote
	- Bấm vào 1 note sẽ hiển thị nội dung note tương ứng
	- Bấm vào 1 tag sẽ hiển thị những note chưa tag đó
	- Bấm vào nút Addnote sẽ hiển thị ra cửa sổ Thêm note
	- Bấm vào nút Allnote sẽ hiển thị tất cả các note đã có
	- Bấm vào nút StatisticTag hiển thị cửa sổ thống kê
	- Notifycaton:
		+ Exit: Thoát	
		+ Viewnotes: Hiển thị listTag, listNote
		+ Viewstatitic: Hiển thị cửa sổ thống kê
		
** Additional flow:	
	- Cửa sổ Thêm note, cửa sổ hiển thị nội dung note:
		+ Nội dung Tag không được rỗng
		+ Tên note không được rỗng
	- Khi bấm vào 1 note thì hiển thị nội dung note và có thể thay đổi nội dung của note
		-> Cập nhật
	- Ribbon -> File -> Close: Đóng cửa sổ
					
** Link Youtube:
	https://youtu.be/md0Q_Dotrqc
	
** Link Reponsitory:
	https://thanhphi14@bitbucket.org/thanhphi14/windev.git